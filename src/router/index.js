import baseRoutes from '@common/router/routes';
import { t } from '@/locales';
import Home from '../views/index.vue';
import FuncPage from '../views/pages/func-page.vue';
import MicroPage from '../views/pages/config-page.vue';
// 功能配置路由名称
export const ConfigRouteName = 'ConfigItem';
/**
 * 基础路由
 * 如果有业务需要，可以自行调整
 * 固定路由：Home(/),Login(/login)
 */
const routes = [
  {
    path: '/',
    name: 'Home',
    redirect: '/config',
  },
  {
    path: '/config', // 子系统
    name: 'Config',
    text: t('menu.home'),
    menu: true,
    component: Home,
  },
  {
    path: '/config/:funcId', // 功能配置页面
    name: ConfigRouteName,
    component: FuncPage,
  },
  {
    path: '/init', // 微应用初始页面
    name: 'Init',
    component: MicroPage,
  },
  ...baseRoutes,
];
export default routes;

/**
 * 自定义路由History
 * @returns History
 */
// export function createRouterHistory() {
//   return createMemoryHistory();
// }

/**
 * 自定义路由守卫
 */
// export function createRouterGuard(router) {
//   // 业务逻辑
// }

/**
 * 路由白名单
 */
export const whiteRoutes = ['Init'];
