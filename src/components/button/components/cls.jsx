import { ref } from 'vue';
import { Modal } from '@jecloud/ui';
import Cls from './cls.vue';

/**
 *
 * @param {Object} options
 * @param {String} title 标题
 * @param {Function} callback 回调函数
 *
 */

export function ClsModal(options) {
  const { title, callback, selectData } = options;
  const recordData = {
    title: title ? title : '按钮样式',
    ...selectData,
  };
  const dataRef = ref(recordData);
  const ClsRef = ref();
  let modal = null; // 窗口对象

  // 确认操作
  const okButton = function () {
    const { formState } = ClsRef.value;
    callback?.({ ...formState });
    modal.close();
  };
  // 初始选中值
  const onShow = function ({ $modal }) {
    modal = $modal;
  };
  const ClsSolt = () => {
    return (
      <Cls
        ref={ClsRef}
        selectData={dataRef.value}
        bodyBorder
        allowDeselect={false}
        onCellDblclick={okButton}
      />
    );
  };

  return Modal.window({
    title: dataRef.value.title,
    maximizable: false,
    width: '710',
    height: 'auto',
    bodyStyle: { padding: '0 20px' },
    headerStyle: { height: '60px' },
    okButton: {
      closable: false,
      handler: okButton,
    },
    onShow,
    cancelButton: true,
    slots: {
      default: ClsSolt,
    },
  });
}
