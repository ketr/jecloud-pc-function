import { useFuncStore } from '@/store/func-store';

export function useFormFields() {
  const funcStore = useFuncStore();
  /**
   * 更新数据，index.vue使用
   * @param {*} param0
   */
  const updateFuncStore = ({ fieldsetArr, attachArr }) => {
    funcStore.groupFields = fieldsetArr;
    funcStore.attachFields = attachArr;
  };

  return { updateFuncStore, funcStore };
}
