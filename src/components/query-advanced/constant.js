export const QUERYADVANCED_OPTIONS_TYPE = {
  textfield: [
    { value: 'like', label: '模糊' },
    { value: '=', label: '精确' },
    { value: 'in', label: '包含' },
    { value: 'notIn', label: '不包含' },
    { value: 'notNull', label: '非空' },
  ],
  textarea: [
    { value: 'like', label: '模糊' },
    { value: '=', label: '精确' },
    { value: 'in', label: '包含' },
    { value: 'notIn', label: '不包含' },
    { value: 'notNull', label: '非空' },
  ],
  textcode: [
    { value: 'like', label: '模糊' },
    { value: '=', label: '精确' },
    { value: 'in', label: '包含' },
    { value: 'notIn', label: '不包含' },
    { value: 'notNull', label: '非空' },
  ],
  cbbfield: [
    { value: 'like', label: '模糊' },
    { value: '=', label: '精确' },
    { value: 'in', label: '包含' },
    { value: 'notIn', label: '不包含' },
    { value: 'notNull', label: '非空' },
  ],
  rgroup: [
    { value: '=', label: '精确' },
    { value: 'in', label: '包含' },
    { value: 'notIn', label: '不包含' },
    { value: 'notNull', label: '非空' },
  ],
  cgroup: [
    { value: 'like', label: '模糊' },
    { value: '=', label: '精确' },
    { value: 'in', label: '包含' },
    { value: 'notIn', label: '不包含' },
    { value: 'notNull', label: '非空' },
  ],
  numberfield: [
    { value: 'between', label: '区间' },
    { value: '=', label: '精确' },
    { value: 'like', label: '模糊' },
    { value: '>', label: '大于' },
    { value: '>=', label: '大于等于' },
    { value: '<', label: '小于' },
    { value: '<=', label: '小于等于' },
    { value: 'notIn', label: '不包含' },
    { value: 'notNull', label: '非空' },
  ],
  barfield: [
    { value: 'between', label: '区间' },
    { value: '=', label: '精确' },
    { value: 'like', label: '模糊' },
    { value: '>', label: '大于' },
    { value: '>=', label: '大于等于' },
    { value: '<', label: '小于' },
    { value: '<=', label: '小于等于' },
    { value: 'notIn', label: '不包含' },
    { value: 'notNull', label: '非空' },
  ],
  treessfield: [
    { value: 'like', label: '模糊' },
    { value: '=', label: '精确' },
    { value: 'in', label: '包含' },
    { value: 'notIn', label: '不包含' },
  ],
  gridssfield: [
    { value: 'like', label: '模糊' },
    { value: '=', label: '精确' },
    { value: 'in', label: '包含' },
    { value: 'notIn', label: '不包含' },
    { value: 'notNull', label: '非空' },
  ],
  searchfield: [
    { value: 'like', label: '模糊' },
    { value: '=', label: '精确' },
    { value: 'in', label: '包含' },
    { value: 'notIn', label: '不包含' },
    { value: 'notNull', label: '非空' },
  ],
  vueuserfield: [
    { value: 'like', label: '模糊' },
    { value: '=', label: '精确' },
    { value: 'in', label: '包含' },
    { value: 'notIn', label: '不包含' },
    { value: 'notNull', label: '非空' },
  ],
  starfield: [
    { value: '=', label: '精确' },
    { value: '>', label: '大于' },
    { value: '>=', label: '大于等于' },
    { value: '<', label: '小于' },
    { value: '<=', label: '小于等于' },
    { value: 'notIn', label: '不包含' },
    { value: 'notNull', label: '非空' },
  ],
  datefield: [
    { value: 'between', label: '区间' },
    { value: '=', label: '精确' },
    { value: 'in', label: '包含' },
    { value: '>', label: '大于' },
    { value: '>=', label: '大于等于' },
    { value: '<', label: '小于' },
    { value: '<=', label: '小于等于' },
    { value: 'notIn', label: '不包含' },
    { value: 'notNull', label: '非空' },
  ],
  rangedatefield: [
    { value: 'between', label: '区间' },
    { value: '=', label: '精确' },
    { value: '>', label: '大于' },
    { value: '>=', label: '大于等于' },
    { value: '<', label: '小于' },
    { value: '<=', label: '小于等于' },
    { value: 'notIn', label: '不包含' },
    { value: 'notNull', label: '非空' },
  ],
  clocktimefield: [
    { value: 'between', label: '区间' },
    { value: '=', label: '精确' },
    { value: '>', label: '大于' },
    { value: '>=', label: '大于等于' },
    { value: '<', label: '小于' },
    { value: '<=', label: '小于等于' },
    { value: 'notIn', label: '不包含' },
    { value: 'notNull', label: '非空' },
  ],
};
