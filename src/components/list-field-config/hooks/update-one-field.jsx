import { ref } from 'vue';
import { Modal } from '@jecloud/ui';
import UpdataOneFieldValue from '../components/update-one-field-value.vue';

/**
 *
 * @param {Object} options
 * @param {String} title 标题
 * @param {Function} callback 回调函数
 *
 */

export function UpdataOneFieldModal(options) {
  const { title, recordData } = options;
  const data = {
    title: title ? title : '批量修改数据',
    ...recordData,
  };
  const { callback } = options;
  const dataRef = ref(data);
  const UpdateOneFieldRef = ref();
  let modal = null; // 窗口对象

  // 确认操作
  const okButton = function () {
    const { fieldName, isOnlyChecked, value } = UpdateOneFieldRef.value;
    callback?.({ fieldName, isOnlyChecked, value });
    modal.close();
  };
  // 初始选中值
  const onShow = function ({ $modal }) {
    modal = $modal;
  };
  const ClsSolt = () => {
    return (
      <UpdataOneFieldValue
        ref={UpdateOneFieldRef}
        recordData={dataRef.value}
        bodyBorder
        allowDeselect={false}
        onCellDblclick={okButton}
      />
    );
  };

  return Modal.window({
    title: dataRef.value.title,
    maximizable: false,
    width: '460',
    height: 'auto',
    bodyStyle: { padding: '0 20px' },
    headerStyle: { height: '60px' },
    okButton: {
      closable: false,
      handler: okButton,
    },
    onShow,
    cancelButton: true,
    slots: {
      default: ClsSolt,
    },
  });
}
