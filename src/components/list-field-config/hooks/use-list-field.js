import { useFuncStore } from '@/store/func-store';
import { useFuncConfig } from '@/hooks/use-func-config';
import { Hooks } from '@jecloud/ui';

export function useListField({ props, context }) {
  const funcStore = useFuncStore();
  // 表单数据
  const formState = Hooks.useModelValue({ props, context, key: 'recordData' });
  /**
   * 打开表单字段配置
   */
  const showFieldConfig = ({ props }) => {
    const { showFuncConfig } = useFuncConfig();
    showFuncConfig({
      key: 'FuncFormField',
      funcId: formState.value.RESOURCECOLUMN_FUNCINFO_ID,
      code: formState.value.RESOURCECOLUMN_CODE,
      props,
    });
  };

  return {
    funcStore,
    formState,
    showFieldConfig,
  };
}
