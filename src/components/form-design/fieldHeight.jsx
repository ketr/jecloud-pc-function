/* eslint-disable vue/no-mutating-props */
import { ref, reactive, defineComponent, computed, nextTick, onMounted, watch } from 'vue';
import { useDraggable } from './hooks/use-draggable';
import { Form, Switch, Select, Col } from '@jecloud/ui';
import { encode, decode, getDDItemInfo } from '@jecloud/utils';
import FuncField from './hooks/func/func-field';
import VueEvent from '@/helper/event';
import elementResizeDetectorMaker from 'element-resize-detector';
import { columns as columnOptions } from './hooks/config';
import { useFormField } from './hooks/func/use-form-field';
import { FuncFieldTypeEnum } from './hooks/func/func-field-enum';

export default defineComponent({
  name: 'DargContainerField',
  components: { Col },
  inheritAttrs: false,
  props: {
    data: {
      type: Object,
      default() {
        return {};
      },
    },
    columns: { type: String, default: '1' },
    labelWidth: { type: [String, Number] },
  },
  setup(props) {
    const { dragClass, removeFn } = useDraggable({ owner: 'field', props });
    let field = new FuncField(props.data);
    let colRef = ref('');
    let width = ref('');
    let peiseRef = ref('');
    let config = decode((props.data.RESOURCEFIELD_OTHERCONFIG || undefined) ?? '{}') ?? {};
    let otherCfgs = reactive({
      titleWidth: config.titleWidth || '', //组内标题宽
      bgColor: config.bgColor || '', //分组框背景
      titleColor: config.titleColor || '', //标题色
      titleBgColor: config.titleBgColor || '', //标题背景色
      borderColor: config.borderColor || '', //边框色
    });
    const formItemProps = reactive({});
    // 标签宽度
    if (field.labelWidth) {
      formItemProps.labelCol = { style: { width: `${field.labelWidth}px` } };
    }
    // 监听变化
    watch(
      () => props.data,
      (newVal) => {
        if (newVal) {
          field = new FuncField(newVal);
          // 标签宽度
          if (field.labelWidth) {
            formItemProps.labelCol = { style: { width: `${field.labelWidth}px` } };
          }
        }
      },
      { deep: true },
    );

    // 监听变化
    watch(
      () => otherCfgs,
      (newVal) => {
        if (newVal) {
          config = decode(newVal ?? '{}') ?? {};

          Object.keys(otherCfgs).forEach((item) => {
            otherCfgs[item] = config[item];
          });
        }
      },
    );

    let spanNumber = computed(() => {
      if (props.data.RESOURCEFIELD_COLSPAN >= props.columns) {
        // eslint-disable-next-line vue/no-mutating-props
        props.data.RESOURCEFIELD_COLSPAN = props.columns;
      }

      // 子功能独占一行
      if (['child', 'workflowhistory'].includes(props.data.RESOURCEFIELD_XTYPE)) {
        return 24;
      } else {
        return Math.ceil((props.data.RESOURCEFIELD_COLSPAN / props.columns) * 24);
      }
    });

    let style = ref({});
    let isShowColorScheme = ref(false);
    const showColorScheme = () => {
      let obj = peiseRef.value.getBoundingClientRect();
      if (window.innerHeight - obj.top < 300) {
        style.value.top = obj.top - 300 + 'px';
      } else {
        style.value.top = obj.top + 30 + 'px';
      }

      style.value.left = obj.left - 230 + 'px';

      isShowColorScheme.value = !isShowColorScheme.value;
    };

    const hideColorScheme = (e) => {
      isShowColorScheme.value = false;
      e.stopPropagation();
    };

    const colorSchemeStopPropagation = (e) => {
      e.stopPropagation();
    };

    const erd = elementResizeDetectorMaker();
    onMounted(() => {
      erd.listenTo(colRef.value, (element) => {
        nextTick(() => {
          // 实时获取宽度，以此显示决定是否显示某些元素
          width.value = element.offsetWidth;
        });
      });
    });

    const handleChangeAttrValue = (key, value) => {
      otherCfgs[key] = value;

      // eslint-disable-next-line vue/no-mutating-props
      props.data.RESOURCEFIELD_OTHERCONFIG = encode(otherCfgs);
    };
    // 点击编辑进入表单详细配置
    const showFuncConfigField = (e) => {
      VueEvent.emit('showEditFormFieldConfig', props.data);
      e.preventDefault();
    };

    const handleChangeAttrValue3 = (key, value) => {
      if (value >= props.columns) {
        // eslint-disable-next-line vue/no-mutating-props
        props.data[key] = props.columns;
      } else {
        // eslint-disable-next-line vue/no-mutating-props
        props.data[key] = value;
      }
    };

    const handleClick = () => {
      VueEvent.emit('showEditFormFieldConfig', props.data);
    };
    const clickTechGrey = () => {
      // 框内背景颜色: #FFFFFF 标题色：#3F3F3F 标题背景色：#F5F5F5 边框色：#D9D9D9
      otherCfgs.bgColor = '#FFFFFF';
      otherCfgs.titleColor = '#3F3F3F';
      otherCfgs.titleBgColor = '#EFEFEF';
      otherCfgs.borderColor = '#EFEFEF';
      // eslint-disable-next-line vue/no-mutating-props
      props.data.RESOURCEFIELD_OTHERCONFIG = encode(otherCfgs);
    };
    let mouseEnterActived = ref('');
    // 当鼠标移上去的时候，要显示配置
    const handleMouseEnter = (itemField) => {
      mouseEnterActived.value = itemField.id;
    };
    const getClass = (data) => {
      return getDDItemInfo('JE_FUNC_FIELD_XTYPE', data.RESOURCEFIELD_XTYPE)?.icon || '';
    };

    return () => (
      <Col span={spanNumber.value} key={props.data.id} data-id={props.data.id} data-type="field">
        {['child', 'workflowhistory'].includes(props.data.RESOURCEFIELD_XTYPE) ? (
          <div
            ref={colRef}
            class={['container-field', dragClass, isShowColorScheme.value ? 'filter' : '']}
            style={{
              background:
                otherCfgs.titleBgColor || props.data.RESOURCEFIELD_XTYPE == 'child'
                  ? '#bbefbd'
                  : '#BBCAEF',
              borderColor:
                otherCfgs.borderColor || props.data.RESOURCEFIELD_XTYPE == 'child'
                  ? '#12CB61'
                  : '#678AE1',
            }}
          >
            {width.value > 120 ? (
              <span style={{ display: 'flex', flex: '1', width: 0, 'padding-left': '8px' }}>
                <i onClick={handleClick} class={['icon', getClass(props.data)]}></i>
                <span
                  class={{ text: true }}
                  title={props.data.RESOURCEFIELD_NAME}
                  style={{ color: otherCfgs.titleColor, cursor: 'move' }}
                >
                  {props.data.RESOURCEFIELD_NAME}
                  <span style="font-size: 12px">（不支持拖拽到分组框内）</span>
                </span>
              </span>
            ) : null}

            <div class="actions">
              {/* <span class="peise" ref={peiseRef} onClick={showColorScheme}>
                配色方案 <i class="fal fa-angle-down "></i> &nbsp;&nbsp;
                {isShowColorScheme.value ? (
                  <div class="dropmenuWrap" onClick={hideColorScheme}></div>
                ) : null}
                {isShowColorScheme.value ? (
                  <div
                    class="dropmenu"
                    style={{ top: style.value.top, left: style.value.left }}
                    onClick={colorSchemeStopPropagation}
                  >
                    <Form.Item label="框内背景颜色" name="bgColor">
                      <Color
                        onChange={(e) => handleChangeAttrValue('bgColor', e.target.value)}
                        onSelect={(e) => handleChangeAttrValue('bgColor', e.value)}
                        v-model:value={otherCfgs.bgColor}
                        style={{ width: '150px' }}
                      />
                    </Form.Item>
                    <Form.Item label="边框颜色" name="borderColor">
                      <Color
                        onChange={(e) => handleChangeAttrValue('borderColor', e.target.value)}
                        onSelect={(e) => handleChangeAttrValue('borderColor', e.value)}
                        v-model:value={otherCfgs.borderColor}
                        style={{ width: '150px' }}
                      />
                    </Form.Item>
                    <Form.Item label="标题颜色" name="titleColor">
                      <Color
                        onChange={(e) => handleChangeAttrValue('titleColor', e.target.value)}
                        onSelect={(e) => handleChangeAttrValue('titleColor', e.value)}
                        v-model:value={otherCfgs.titleColor}
                        style={{ width: '150px' }}
                      />
                    </Form.Item>
                    <Form.Item label="标题背景颜色" name="titleBgColor">
                      <Color
                        onChange={(e) => handleChangeAttrValue('titleBgColor', e.target.value)}
                        onSelect={(e) => handleChangeAttrValue('titleBgColor', e.value)}
                        v-model:value={otherCfgs.titleBgColor}
                        style={{ width: '150px' }}
                      />
                    </Form.Item>

                    <div style="text-align: center">
                      <Button type="text" style="text-align: center" onClick={clickTechGrey}>
                        科技灰配色方案
                      </Button>
                    </div>
                  </div>
                ) : null}
              </span> */}
              <i class="fal fa-times remove-icon" onClick={removeFn}></i>
            </div>
          </div>
        ) : null}

        {props.data.RESOURCEFIELD_XTYPE !== 'child' &&
        props.data.RESOURCEFIELD_XTYPE !== 'workflowhistory' ? (
          <div
            ref={colRef}
            class={[
              'container-field',
              dragClass,
              'common-css',
              mouseEnterActived.value == field.id ? 'mouseEnterBorder' : 'mouseoverBgColor',
            ]}
            onmouseenter={() => handleMouseEnter(field)}
            onmouseleave={() => (mouseEnterActived.value = '')}
          >
            {mouseEnterActived.value == field.id ? (
              <div class={['common-field-css']}>
                <div
                  style="cursor:pointer;font-size: 14px;color: #006ed7;"
                  onClick={showFuncConfigField}
                >
                  编辑
                </div>
                <div>
                  <Switch
                    defaultChecked={field.required}
                    mode="checkbox"
                    v-model:value={props.data.RESOURCEFIELD_REQUIRE}
                    label={width.value > 300 ? '必填' : null}
                  ></Switch>
                  <Switch
                    mode="checkbox"
                    defaultChecked={field.disabled}
                    v-model:value={props.data.RESOURCEFIELD_READONLY}
                    label={width.value > 300 ? '只读' : null}
                  ></Switch>
                  &nbsp;&nbsp;
                  <span style="margin-right: 8px">列</span>
                  <Select
                    allowClear={false}
                    v-model:value={props.data.RESOURCEFIELD_COLSPAN}
                    size="small"
                    options={columnOptions}
                    onChange={(val) => handleChangeAttrValue3('RESOURCEFIELD_COLSPAN', val)}
                  />
                  <i
                    class="fal fa-times remove-icon"
                    style="margin-left: 10px;cursor:pointer"
                    onClick={removeFn}
                  ></i>
                </div>
              </div>
            ) : null}
            <Form.Item
              name={field.name}
              colon={false}
              htmlFor=""
              labelText={field.label}
              labelAlign={field.labelAlign}
              {...formItemProps}
              v-slots={{
                label: () => {
                  // 字段对应组件
                  const component = FuncFieldTypeEnum.getComponent(field?.xtype);
                  if (component && field.label && !field.labelHidden) {
                    return (
                      <span
                        style={{
                          color: field.labelColor,
                          width: `${props.labelWidth}px`,
                          'word-wrap': 'break-word',
                          display: 'inline-block',
                          'word-break': 'break-all',
                          overflow: 'hidden',
                          'white-space': 'break-spaces',
                          cursor: 'move',
                          padding: '0 8px 0 0',
                        }}
                      >
                        {field.required ? (
                          <span style="color:#ff4d4f;margin-right: 4px;">*</span>
                        ) : null}
                        {field.label}
                      </span>
                    );
                  } else {
                    // 设置了标签宽度需要占位
                    if (field.labelWidth) {
                      return <span></span>;
                    }
                  }
                },
                default: () => {
                  return useFormField({
                    field,
                  });
                },
              }}
              style="margin-bottom:0px;flex:1;"
            ></Form.Item>
          </div>
        ) : null}
      </Col>
    );
  },
});
