/**
 * 字段类型
 * @enum
 */

export const FuncFieldTypeEnum = Object.freeze({
  /**
   * 文本框
   */
  INPUT: { type: 'textfield', text: '文本框', component: 'JeInput' },
  /**
   * 数值框
   */
  INPUT_NUMBER: { type: 'numberfield', text: '数值框', component: 'JeInputNumber' },
  /**
   * 编号
   */
  INPUT_CODE: { type: 'textcode', text: '编号', component: 'JeInput' },
  /**
   * 文本域
   */
  TEXTAREA: { type: 'textarea', text: '文本域', component: 'JeInputTextarea' },
  /**
   * 单选框
   */
  RADIO_GROUP: { type: 'rgroup', text: '单选框', component: 'JeRadioGroup' },
  /**
   * 复选框
   */
  CHECKBOX_GROUP: { type: 'cgroup', text: '复选框', component: 'JeCheckboxGroup' },
  /**
   * 下拉框
   */
  SELECT: { type: 'cbbfield', text: '下拉框', component: 'JeSelect' },
  /**
   * HTML编辑器
   */
  EDITOR_HTML: { type: 'ckeditor', text: 'HTML编辑器', component: 'JeEditorHtml' },
  /**
   * 附件
   */
  UPLOAD_INPUT: { type: 'uxfilefield', text: '附件', component: 'JeUpload' },
  /**
   * 多附件
   */
  UPLOAD: { type: 'uxfilesfield', text: '多附件', component: 'JeUploadFiles' },
  /**
   * 图片选择器
   */
  UPLOAD_IMAGE: { type: 'imagepickerfield', text: '图片选择器', component: 'JeUploadImage' },
  /**
   * 日期
   */
  DATE_PICKER: { type: 'datefield', text: '日期', component: 'JeDatePicker' },
  /**
   * 日期区间
   */
  DATE_RANGE_PICKER: { type: 'rangedatefield', text: '日期区间', component: 'JeDateRangePicker' },
  /**
   * 时间
   */
  TIME_PICKER: { type: 'clocktimefield', text: '时间', component: 'JeTimePicker' },
  /**
   * 时间区间
   */
  TIME_RANGE_PICKER: { type: 'rangetimefield', text: '时间区间', component: 'JeTimeRangePicker' },
  /**
   * 查询选择
   */
  INPUT_SELECT_GRID: { type: 'gridssfield', text: '查询选择', component: 'JeInputSelectGrid' },
  /**
   * 树形选择
   */
  INPUT_SELECT_TREE: { type: 'treessfield', text: '树形选择', component: 'JeInputSelectTree' },
  /**
   * 人员选择
   */
  INPUT_SELECT_USER: { type: 'vueuserfield', text: '人员选择', component: 'JeInputSelectUser' },
  /**
   * 颜色选择
   */
  INPUT_SELECT_COLOR: { type: 'colorfield', text: '颜色选择', component: 'JeColor' },
  /**
   * 图标选择
   */
  INPUT_SELECT_ICON: { type: 'iconfield', text: '图标选择', component: 'JeIcon' },
  /**
   * 查询
   */
  SEARCH: { type: 'search', text: '查询', component: 'JeSearch' },
  /**
   * 智能查询
   */
  SEARCH_GRID: { type: 'searchfield', text: '智能查询', component: 'JeInputSelectGrid' },
  /**
   * 展示字段
   */
  DISPLAY: { type: 'displayfield', text: '展示字段', component: 'JeInputDisplay' },
  /**
   * 评星
   */
  STAR: { type: 'starfield', text: '评星', component: 'JeRate' },
  /**
   * 拼音
   */
  PINYIN: { type: 'pinyinfield', text: '拼音', component: 'JePinyin' },
  /**
   * 进度条
   */
  PROGRESS: { type: 'barfield', text: '进度条', component: 'JeProgress' },
  /**
   * 分组框
   */
  FIELDSET: { type: 'fieldset', text: '分组框', component: 'JeFieldset' },
  /**
   * 代码编辑器
   */
  EDITOR_CODE: { type: 'codeeditor', text: '代码编辑器', component: 'JeEditorCode' },
  /**
   * 子功能集合
   */
  FUNC_CHILD_FIELD: {
    type: 'childfuncfield',
    text: '子功能集合',
    component: 'JeFuncChildrenField',
  },
  /**
   * 数据集合
   */
  JSON_ARRAY_FIELD: { type: 'jsonarrayfield', text: '数据集合', component: 'JeJsonArray' },
  /**
   * 子功能
   */
  FUNC_CHILD: { type: 'child', text: '子功能', component: 'JeFuncChildren' },

  /**
   * 审批记录
   */
  WORKFLOW_HISTORY: { type: 'workflowhistory', text: '审批记录', component: 'JeWorkflowHistory' },
  /**
  /**
   * 获得组件类型
   * @param {*} xtype
   * @returns
   */
  getXtype(xtype) {
    return Object.keys(FuncFieldTypeEnum).find((key) => FuncFieldTypeEnum[key]?.type === xtype);
  },
  /**
   * 获得对应的组件名
   * @param {*} xtype
   * @returns
   */
  getComponent(xtype) {
    return FuncFieldTypeEnum[FuncFieldTypeEnum.getXtype(xtype)]?.component;
  },
});
