import { resolveComponent, h } from 'vue';
import {
  encode,
  doEvents4Sync,
  // split,
  // loadDDItemByCode,
  // getDDCache,
  // DDType,
  // loadDDItemByCode4Tree,
  // omit,
  isNotEmpty,
} from '@jecloud/utils';
import { parseFieldProps, useModelValue } from './util';
import { FuncFieldTypeEnum } from './func-field-enum';
/**
 * 表单字段
 * @param {*} param0
 * @returns
 */
export function useFormField({ field }) {
  let component = FuncFieldTypeEnum.getComponent(field.xtype);

  let props = parseFieldProps({ field });
  const vnode = resolveComponent(component);

  if (component == vnode.name) {
    switch (field.xtype) {
      case FuncFieldTypeEnum.FUNC_CHILD_FIELD.type: // 子功能集合
        // eslint-disable-next-line no-case-declarations
        const [funcCode, fkCode] = props.configInfo?.split(',') || ['', ''];
        Object.assign(props, {
          funcCode,
          fkCode,
          actionColumn: field.otherConfig,
          field,
          autoLoad: false,
        });
        break;
      case FuncFieldTypeEnum.JSON_ARRAY_FIELD.type: // 数据集合
        const jsonArrayConfig = { actionColumn: field.otherConfig };
        const eventColumnRenderCode = field.events['column-render'];
        if (isNotEmpty(eventColumnRenderCode)) {
          jsonArrayConfig['onColumnRender'] = () => {
            return doEvents4Sync({
              eventOptions: { model: {} },
              code: eventColumnRenderCode,
            });
          };
        }
        Object.assign(props, jsonArrayConfig);
        break;
      case FuncFieldTypeEnum.RADIO_GROUP.type: // 单选框
      case FuncFieldTypeEnum.CHECKBOX_GROUP.type: // 复选框
        const { otherConfig } = field;
        const config = {};
        if (otherConfig.layoutStyle == 'gridLayout') {
          // 栅格布局;
          config.wrap = true;
          config.cols = otherConfig.showCloum;
        } else if (otherConfig.layoutStyle == 'singleLayout') {
          // 单行布局
        } else {
          // 流式布局
          config.cols = 0;
          config.wrap = true;
        }
        Object.assign(props, config);
        // config.code = field.configInfo && (split(field.configInfo, ',') && split(field.configInfo, ',')[0]) || '';
        // if (config.code) {
        //   const ddCache = getDDCache(config.code);
        //   config.options = [];
        //   if (ddCache?.type !== DDType.LIST || isNotEmpty(props.querys)) {
        //     loadDDItemByCode4Tree({ code: config.code, querys: props.querys }).then((data) => {
        //       data.map?.((item) => {
        //         Object.assign(omit(item, ['disabled']), {
        //           value: item.code,
        //           label: item.text,
        //         });
        //         config.options.push(item);
        //       });
        //       Object.assign(props, config);
        //     });
        //   } else {
        //     loadDDItemByCode(config.code).then((data) => {
        //       data.map?.((item) => {
        //         Object.assign(item, { value: item.code, label: item.text });
        //         config.options.push(item);
        //       });
        //       Object.assign(props, config);
        //       console.log('props',props)
        //       return h(vnode, { ...props});
        //     })
        //   }
        // }
        break;
    }
    Object.assign(props);
    return h(vnode, { ...props, ...useModelValue({ model: field, key: 'value' }) });
  } else {
    return h(resolveComponent('JeDisplay'), { value: encode(field) });
  }
}
