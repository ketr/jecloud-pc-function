export const dragClass = 'is--draggable';
export const dragGroup = 'form-design-container';
export const draggableClass = `${dragGroup}-draggable-item`;
export const dragGhostClass = `${dragGroup}-sortable-ghost`;
export const provideDrag = dragGroup;
export const columns = ['1', '2', '3', '4', '6'].map((item) => ({ value: item, label: item }));
