// bgColor: config.bgColor || '', //分组框背景
// titleColor: config.titleColor || '', //标题色
// titleBgColor: config.titleBgColor || '', //标题背景色
// borderColor: config.borderColor || '', //边框色

export const FIELD_COLOR_CONFIG0 = [
  {
    bgColor: '#FFFFFF',
    titleColor: '#3F3F3F',
    titleBgColor: '#EFEFEF',
    borderColor: '#EFEFEF',
  },
  {
    bgColor: '#FFFFFF',
    titleColor: '#3F3F3F',
    titleBgColor: '#E9EEFD',
    borderColor: '#E9EEFD',
  },
  {
    bgColor: '#FFFFFF',
    titleColor: '#3F3F3F',
    titleBgColor: '#FCE5D9',
    borderColor: '#FCE5D9',
  },
  {
    bgColor: '#FFFFFF',
    titleColor: '#3F3F3F',
    titleBgColor: '#FAD9D7',
    borderColor: '#FAD9D7',
  },
  {
    bgColor: '#FFFFFF',
    titleColor: '#3F3F3F',
    titleBgColor: '#CFF3E4',
    borderColor: '#CFF3E4',
  },
];

export const FIELD_COLOR_CONFIG1 = [
  {
    bgColor: '#FFFFFF',
    titleColor: '#FFFFFF',
    titleBgColor: '#7A7D80',
    borderColor: '#EFEFEF',
  },
  {
    bgColor: '#F0F4FF',
    titleColor: '#FFFFFF',
    titleBgColor: '#3265F5',
    borderColor: '#3265F5',
  },
  {
    bgColor: '#FFF5EF',
    titleColor: '#FFFFFF',
    titleBgColor: '#F3752D',
    borderColor: '#F3752D',
  },
  {
    bgColor: '#FFF5F5',
    titleColor: '#FFFFFF',
    titleBgColor: '#E34F47',
    borderColor: '#E34F47',
  },
  {
    bgColor: '#EFFFF8',
    titleColor: '#FFFFFF',
    titleBgColor: '#02A863',
    borderColor: '#02A863',
  },
];
