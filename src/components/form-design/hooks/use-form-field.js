import { useFuncStore } from '@/store/func-store';
import { useFuncConfig } from '@/hooks/use-func-config';

export function useFormField({ props }) {
  const funcStore = useFuncStore();
  const funcId = props.recordData.JE_CORE_FUNCINFO_ID;
  /**
   * 打开表单字段配置
   */
  const showFieldConfig = ({ bean, props }) => {
    const { showFuncConfig } = useFuncConfig();
    showFuncConfig({
      key: 'FuncFormField',
      funcId: funcId,
      bean,
      props,
    });
  };

  return {
    funcStore,
    showFieldConfig,
  };
}
