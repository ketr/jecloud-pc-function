import { defineComponent, watch, ref } from 'vue';
import Group from './group';
import FieldHeight from './fieldHeight';
import Field from './field';
import { useDraggable } from './hooks/use-draggable';
import { Row } from 'ant-design-vue';

export default defineComponent({
  name: 'DargContainer',
  components: { Field, Group, Row, FieldHeight },
  props: {
    data: {
      type: Array,
      default() {
        return [];
      },
    },
    columns: { type: String, default: '3' },
    styleType: {
      type: String,
      default: 'FIDELITY',
    },
    formStyle: {
      type: String,
    },
    labelWidth: {
      type: [String, Number],
    },
  },
  setup(props, { expose }) {
    const { dragEl } = useDraggable({ owner: 'container', props });
    const isField = ref(false);
    // 子项数据
    const itemsSlot = ({ data, groupId, columns, labelWidth }) => {
      const items = [];
      data.forEach((item) => {
        // 别删
        // 当 item.groupId 为空字符串，为 null 的时候
        // 判断和 Boolean(groupId) 相等
        let boolean1 = Boolean(groupId);
        let boolean2 = Boolean(item.groupId);

        if (item.groupId == groupId || (boolean1 === false && boolean2 === false)) {
          if (item.type === 'group') {
            items.push(
              <Group
                data={item}
                columns={columns}
                key={item.id}
                v-slots={{
                  default(options) {
                    return itemsSlot({
                      data,
                      groupId: item.id,
                      columns: options.columns,
                      labelWidth,
                    });
                  },
                }}
              ></Group>,
            );
          } else if (props.styleType == 'TYPOGRAPHY') {
            items.push(<Field data={item} columns={columns} key={item.id}></Field>);
          } else {
            items.push(
              <FieldHeight
                data={item}
                columns={columns}
                key={item.id}
                label-width={labelWidth}
              ></FieldHeight>,
            );
          }
        }
      });
      return items;
    };
    expose({
      dragEl,
    });
    // 监听变化
    watch(
      () => props.data,
      (newVal) => {
        if (newVal && newVal[0]) {
          const { type, RESOURCEFIELD_CODE, RESOURCEFIELD_XTYPE } = props.data[0];
          isField.value =
            props.styleType == 'FIDELITY' &&
            type == 'field' &&
            RESOURCEFIELD_CODE != '__workflow_history' &&
            RESOURCEFIELD_XTYPE != 'child';
        }
      },
      { deep: true, immediate: true },
    );
    return () => (
      <Row
        class={[
          'container',
          props.styleType != 'TYPOGRAPHY' ? 'typography-height' : 'only-type-setting',
        ]}
        data-type="container"
        gutter={[4, 4]}
        ref={dragEl}
        style={{
          'row-gap': `${props.formStyle == '0' ? 0 : 12}px`,
          'padding-top': `${isField.value ? '35' : '5'}px`,
        }}
      >
        {itemsSlot({ data: props.data, columns: props.columns, labelWidth: props.labelWidth })}
      </Row>
    );
  },
});
