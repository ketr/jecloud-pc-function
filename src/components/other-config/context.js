import { provide, inject } from 'vue';
const contextKey = 'otherConfigComponent';
/**
 * 设置父组件的传递对象
 * @param {*} state
 */
export function useProvideOtherCOnfig(state) {
  provide(contextKey, state);
}

/**
 * 获取父组件的传递对象
 * @returns
 */
export function useInjectOtherCOnfig() {
  return inject(contextKey, {});
}
