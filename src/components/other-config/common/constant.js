import { defineAsyncComponent } from 'vue';

export const OTHER_CONFIG_CONSTANT = {
  /* 单选框,复选框 */
  RadioCheckboxConfig: defineAsyncComponent({
    delay: 1000 /* 在显示加载组件之前提早毫秒 */,
    loader: () => import('@/components/other-config/config/radio-checkbox-config.vue'),
  }),

  /* 下拉框 */
  SelectConfig: defineAsyncComponent({
    delay: 1000 /* 在显示加载组件之前提早毫秒 */,
    loader: () => import('@/components/other-config/config/select-config.vue'),
  }),

  /* 树形选择,树形选择(大) */
  TreeselectConfig: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/components/other-config/config/treeselect-config.vue'),
  }),

  /* 单附件 */
  FileConfig: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/components/other-config/config/file-config.vue'),
  }),

  /* 多附件 */
  FilesConfig: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/components/other-config/config/files-config.vue'),
  }),

  /* 查询选择,查询选择(大) */
  GridselectConfig: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/components/other-config/config/gridselect-config.vue'),
  }),

  /* */
  SearchfieldConfig: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/components/other-config/config/searchfield-config.vue'),
  }),

  /* 人员选择,人员选(大)*/
  PersonselectConfig: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/components/other-config/config/personselect-config.vue'),
  }),

  /* 子功能集合*/
  ChildfuncfieldConfig: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/components/other-config/config/childfuncfield-config.vue'),
  }),

  /* 分组框*/
  CollapseConfig: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/components/other-config/config/collapse-config.vue'),
  }),

  /* 图片选择器*/
  ImagepickerfieldConfig: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/components/other-config/config/imagepickerfield-config.vue'),
  }),

  /* 子功能*/
  ChildfunConfig: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/components/other-config/config/childfun-config.vue'),
  }),

  /* 日期 日期区间*/
  DatepickerConfig: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/components/other-config/config/datepicker-config.vue'),
  }),

  /* 对象集合*/
  JsonarrayfieldConfig: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/components/other-config/config/jsonarrayfield-config.vue'),
  }),

  CodeeditorConfig: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/components/other-config/config/codeeditor-config.vue'),
  }),
};
export const xtypeConfig = {
  /**
   * 树形选择
   */
  treessfield: 'TreeselectConfig',
  /**
   * 单选框
   */
  rgroup: 'RadioCheckboxConfig',

  /**
   * 复选框
   */
  cgroup: 'RadioCheckboxConfig',

  /**
   * 下拉框
   */
  cbbfield: 'SelectConfig',

  /**
   * 单附件
   */
  uxfilefield: 'FileConfig',

  /**
   * 多附件
   */
  uxfilesfield: 'FilesConfig',

  /**
   * 关联选择
   */
  gridssfield: 'GridselectConfig',

  /**
   * 查询选择
   */
  searchfield: 'SearchfieldConfig',

  /**
   * 人员选择
   */
  vueuserfield: 'PersonselectConfig',

  /**
   * 子功能集合
   */
  childfuncfield: 'ChildfuncfieldConfig',

  /**
   * 分组框
   */
  fieldset: 'CollapseConfig',

  /**
   * 图片选择器
   */
  imagepickerfield: 'ImagepickerfieldConfig',

  /**
   * 子功能
   */
  child: 'ChildfunConfig',

  /**
   * 日期
   */
  datefield: 'DatepickerConfig',

  /**
   * 日期区间
   */
  rangedatefield: 'DatepickerConfig',

  /**
   * 对象集合
   */
  jsonarrayfield: 'JsonarrayfieldConfig',
  /**
   * 代码编辑器
   */
  codeeditor: 'CodeeditorConfig',
};
