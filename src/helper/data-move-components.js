import { defineAsyncComponent } from 'vue';

export const DATA_MOVE_COMPONENTS = {
  Callback: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/components/data-move/components/callback.vue'),
  }),
  StartCondition: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/components/data-move/components/callback.vue'),
  }),
};
