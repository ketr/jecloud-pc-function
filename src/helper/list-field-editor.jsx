import { ref, toRaw } from 'vue';
import { Modal } from '@jecloud/ui';
import ListFieldEdit from '@/components/list-field-config/components/edit-field-config.vue';

/**
 *
 * @param {Object} options
 * @param {String} funcCode 功能编码
 * @param {String} value 回显的数据
 * @param {String} type 类型
 * @param {String} title 标题
 * @param {Function} callback 回调函数
 *
 */

export function ListFieldEditorModal(options) {
  const { callback } = options;
  const dataRef = ref(options);
  const ListFieldRef = ref();
  let modal = null; // 窗口对象

  // 确认操作
  const okButton = function (data) {
    callback?.data;
  };

  // 初始选中值
  const onShow = function ({ $modal }) {
    modal = $modal;
    ListFieldRef.value.showEditFieldConfig({
      ...dataRef.value,
      modal,
    });
  };

  const ListFieldEditSlot = () => {
    return (
      <ListFieldEdit
        ref={ListFieldRef}
        bodyBorder
        allowDeselect={false}
        onCellDblclick={okButton}
      />
    );
  };
  return Modal.window({
    // title: '表单字段配置',
    width: '550px',
    maximizable: false,
    bodyStyle: { padding: '0 20px' },
    headerStyle: { height: '0px' },
    okButton: false,
    cancelButton: false,
    onShow,
    slots: {
      default: ListFieldEditSlot,
    },
  });
}
