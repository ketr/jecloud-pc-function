import { defineAsyncComponent } from 'vue';

export const FORM_FIELD_CONFIG_COMPONENTS = {
  FieldEvent: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/components/form-field-config/components/field-event.vue'),
  }),
  EditFieldConfig: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/components/form-field-config/components/edit-field-config.vue'),
  }),
  ExpressionDesigner: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/components/form-field-config/components/expression-designer.vue'),
  }),
  ExpressionEditorCode: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/components/form-field-config/components/expression-editor-code.vue'),
  }),
  HelpEditorHtml: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/components/form-field-config/components/help-editor-html.vue'),
  }),
  PeopleChooseValueConfig: defineAsyncComponent({
    delay: 1000,
    loader: () =>
      import('@/components/form-field-config/components/people-choose-value-config.vue'),
  }),
  InquireChooseValueConfig: defineAsyncComponent({
    delay: 1000,
    loader: () =>
      import('@/components/form-field-config/components/inquire-choose-value-config.vue'),
  }),
  SmartQueryValueConfig: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/components/form-field-config/components/smart-query-value-config.vue'),
  }),
  DataDictionaryValueConfig: defineAsyncComponent({
    delay: 1000,
    loader: () =>
      import('@/components/form-field-config/components/data-dictionary-value-config.vue'),
  }),
  NumberDesigner: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/components/form-field-config/components/number-designer.vue'),
  }),
  // SqlFilter: defineAsyncComponent({
  //   delay: 1000,
  //   loader: () => import('@/components/form-field-config/components/sql-filter.vue'),
  // }),
  DataDictionaryHighConfig: defineAsyncComponent({
    delay: 1000,
    loader: () =>
      import('@/components/form-field-config/components/data-dictionary-high-config.vue'),
  }),
  ChildFuncFieldSelectFunc: defineAsyncComponent({
    delay: 1000,
    loader: () =>
      import('@/components/form-field-config/components/childfuncfield-select-func.vue'),
  }),
  // ListEditFieldConfig: defineAsyncComponent({
  //   delay: 1000,
  //   loader: () => import('@/components/list-field-config/components/edit-field-config.vue'),
  // }),
};
