import { ADD_FUNCTION_TYPE } from './constant';
/**
 * 格式化字段StringToBoolean（0，1=>false,true）| BooleanToString（false,true => 0，1）
 * @param {String} foramtFieldArr 需要格式化的字段
 * @param {Object} obj 原数据
 * @param {Function} type
 */

export function formateFieldStatus(foramtFieldArr, obj, type) {
  Object.keys(obj).forEach((key) => {
    if (foramtFieldArr.some((item) => item == key)) {
      // 处理返回的字段是0，1,需要变成True,false
      if (type == 'StringToBoolean') {
        obj[key] = obj[key] && obj[key] == 1;
      }
      // 处理True,false,需要变成0,1
      if (type == 'BooleanToString') {
        obj[key] = obj[key] ? '1' : '0';
      }
    }
  });
  return obj;
}

/**
 * 格式化字段入口code-text
 * @param {String} foramtFieldArr 需要格式化的字段
 * @param {Object} obj 原数据
 * @param {Function} type StringToBoolean（0，1=>false,true）| BooleanToString（false,true => 0，1）
 */

export function formateCodeToText(code) {
  if (!code) return '';
  const item = ADD_FUNCTION_TYPE.find((item) => item.value === code);
  return (item && item.label) || '';
}

/**
 * 封装搜索高亮的逻辑
 * @param {String} searchValue
 * @param {string} targeText
 * @return String
 */
const heightColor = 'red';
// TODO 高亮的颜色现在是红色，如果需要更改的话，将heightColor更改就好
export function formatHightText(searchValue, targeText) {
  let text = targeText;
  if (!targeText) return '';
  if (!searchValue) return targeText;
  const re = new RegExp(searchValue, '');
  let replaceString = `<span style="color:${heightColor};">${searchValue}</span>`;
  text = text.replace(re, replaceString);
  return text;
}

/**
 * 比较两个对象不同的属性
 * @param {String} newObj
 * @param {string} oldObj
 * @return Array
 */

export function CompareObjectKeyDiff(newObj = {}, oldObj = {}) {
  let keyFound = {};
  Object.keys(newObj).forEach((key) => {
    if (key != 'SY_ORDERINDEX') {
      newObj[key] = newObj[key] ? newObj[key] : '';
      oldObj[key] = oldObj[key] ? oldObj[key] : '';
    }

    if (newObj[key] !== oldObj[key]) {
      return (keyFound[key] = newObj[key]);
    }
  });
  delete keyFound['_X_ROW_KEY'];
  return keyFound;
}
