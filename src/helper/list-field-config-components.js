import { defineAsyncComponent } from 'vue';

export const LIST_FIELD_CONFIG_COMPONENTS = {
  FieldEvent: defineAsyncComponent({
    delay: 1000,
    loader: () => import('../components/list-field-config/components/field-event.vue'),
  }),
  AddNormalField: defineAsyncComponent({
    delay: 1000,
    loader: () => import('../components/list-field-config/components/add-normal-field.vue'),
  }),
  EditFieldConfig: defineAsyncComponent({
    delay: 1000,
    loader: () => import('../components/list-field-config/components/edit-field-config.vue'),
  }),
  MoreButtonConfig: defineAsyncComponent({
    delay: 1000,
    loader: () => import('../components/list-field-config/components/more-button-config.vue'),
  }),
  UpdateOneFieldValue: defineAsyncComponent({
    delay: 1000,
    loader: () => import('../components/list-field-config/components/update-one-field-value.vue'),
  }),
};
