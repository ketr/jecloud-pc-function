import { defineAsyncComponent } from 'vue';

export const FUNCTION_CHILD_COMPONENTS = {
  AddChild: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/components/function-child/components/add-child.vue'),
  }),
};
