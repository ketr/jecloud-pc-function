import { CleanFunctionCache } from './../api/index';
import { Modal } from '@jecloud/ui';

// 点击清空缓存的按钮
export function cleanCache(param) {
  const params = param || {};
  CleanFunctionCache(params)
    .then((res) => {
      Modal.message(res.message, 'success');
    })
    .catch((err) => {
      Modal.notice(err.message, 'error');
    });
}
