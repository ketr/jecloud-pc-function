import { defineAsyncComponent } from 'vue';

export const FUNCTION_MODULES = {
  // 子系统操作的按钮需要引入的组件
  FUNCTION_TABLE_OPERATE: {
    ModalAdd: defineAsyncComponent({
      delay: 1000 /* 在显示加载组件之前提早毫秒 */,
      loader: () => import('../views/system/modal/model-add/add-function-modal/index.vue'),
    }),
    ModalCopy: defineAsyncComponent({
      loader: () => import('./../views/system/modal/modal-copy.vue'),
      delay: 1000 /* 在显示加载组件之前提早毫秒 */,
    }),
    ModalUpdate: defineAsyncComponent({
      delay: 1000 /* 在显示加载组件之前提早毫秒 */,
      loader: () => import('./../views/system/modal/modal-update.vue'),
    }),
    // 这个页面属于废弃页面，待稳定之后要删除
    // ModalEmpty: defineAsyncComponent({
    //   delay: 1000 /* 在显示加载组件之前提早毫秒 */,
    //   loader: () => import('../views/system/modal/modal-emptyTODO.vue'),
    // }),
    // 删除原来采用的是Modal,现在采用的是弹窗，所哟这个页面是不需要的，需要delete
    // ModalDelete: defineAsyncComponent({
    //   delay: 1000 /* 在显示加载组件之前提早毫秒 */,
    //   loader: () => import('../views/system/modal/modal-deleteTODO.vue'),
    // }),
    AddModal: defineAsyncComponent({
      delay: 1000 /* 在显示加载组件之前提早毫秒 */,
      loader: () => import('./../views/system/modal/model-add/add-modal.vue'),
    }),
    ModalSystem: defineAsyncComponent({
      delay: 1000 /* 在显示加载组件之前提早毫秒 */,
      loader: () => import('./../views/system/modal/model-add/system-add.vue'),
    }),
  },
  // 子系统入口页面左上角添加功,不同类型对应的组件
  ADD_FUNCTION_TYPE: {
    FunctionAdd: defineAsyncComponent({
      delay: 1000 /* 在显示加载组件之前提早毫秒 */,
      loader: () =>
        import('../views/system/modal/model-add/add-function-modal/compontents/function-add.vue'),
    }),
  },
};

// 双击子系统列表进入功能配置中的各个组件
export const FUNCTION_COMPONTENT = {
  FunctionBase: defineAsyncComponent({
    delay: 1000 /* 在显示加载组件之前提早毫秒 */,
    loader: () => import('../components/function-base/index.vue'),
  }),
  FunctionChild: defineAsyncComponent({
    delay: 1000 /* 在显示加载组件之前提早毫秒 */,
    loader: () => import('../components/function-child/index.vue'),
  }),
  DataMove: defineAsyncComponent({
    loader: () => import('../components/data-move/index.vue'),
    delay: 1000 /* 在显示加载组件之前提早毫秒 */,
  }),
  PluginAdd: defineAsyncComponent({
    delay: 1000 /* 在显示加载组件之前提早毫秒 */,
    loader: () => import('../components/plugin-add/index.vue'),
  }),
  UseInstruction: defineAsyncComponent({
    delay: 1000 /* 在显示加载组件之前提早毫秒 */,
    loader: () => import('../components/use-instruction/index.vue'),
  }),
  FunctionHelp: defineAsyncComponent({
    delay: 1000 /* 在显示加载组件之前提早毫秒 */,
    loader: () => import('../components/function-help/index.vue'),
  }),
  ListBaseConfig: defineAsyncComponent({
    delay: 1000 /* 在显示加载组件之前提早毫秒 */,
    loader: () => import('../components/list-base-config/index.vue'),
  }),
  ListFieldConfig: defineAsyncComponent({
    delay: 1000 /* 在显示加载组件之前提早毫秒 */,
    loader: () => import('../components/list-field-config/index.vue'),
  }),
  ListItemCunstom: defineAsyncComponent({
    delay: 1000 /* 在显示加载组件之前提早毫秒 */,
    loader: () => import('../components/list-item-custom/index.vue'),
  }),
  ListPrint: defineAsyncComponent({
    delay: 1000 /* 在显示加载组件之前提早毫秒 */,
    loader: () => import('../components/list-print/index.vue'),
  }),
  FormBaseConfig: defineAsyncComponent({
    delay: 1000 /* 在显示加载组件之前提早毫秒 */,
    loader: () => import('../components/form-base-config/index.vue'),
  }),
  FormFieldConfig: defineAsyncComponent({
    delay: 1000 /* 在显示加载组件之前提早毫秒 */,
    loader: () => import('../components/form-field-config/index.vue'),
  }),
  FormDesign: defineAsyncComponent({
    delay: 1000 /* 在显示加载组件之前提早毫秒 */,
    loader: () => import('../components/form-design/index.vue'),
  }),
  FormPrint: defineAsyncComponent({
    delay: 1000 /* 在显示加载组件之前提早毫秒 */,
    loader: () => import('../components/form-print/index.vue'),
  }),
  Button: defineAsyncComponent({
    delay: 1000 /* 在显示加载组件之前提早毫秒 */,
    loader: () => import('../components/button/index.vue'),
  }),
};
export const RIGHT_FUNCTION_LIST = {
  ...FUNCTION_COMPONTENT,
  ListEditFieldConfig: defineAsyncComponent({
    delay: 1000,
    loader: () => import('../components/list-field-config/components/edit-field-config.vue'),
  }),
  FormEditFieldConfig: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/components/form-field-config/components/edit-field-config.vue'),
  }),
  // QueryStrategy: defineAsyncComponent({
  //   delay: 1000,
  //   loader: () => import('@/components/query-strategy/index.vue'),
  // }),
  // QueryAdvanced: defineAsyncComponent({
  //   delay: 1000,
  //   loader: () => import('@/components/query-advanced/index.vue'),
  // }),
  // SqlFilter: defineAsyncComponent({
  //   delay: 1000,
  //   loader: () => import('@/components/form-field-config/components/sql-filter.vue'),
  // }),
  // ListenerEditor: defineAsyncComponent({
  //   delay: 1000,
  //   loader: () => import('@/components/query-advanced/index.vue'),
  // }),
};
