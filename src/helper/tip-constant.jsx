export const ACTION_UNITIPLE = () => {
  return (
    <div>
      用于定义后端交互的controller，通过指定后端controller定义的request
      mapping，用户可以自定义增删改查的逻辑。平台默认会指定为"/je"。
    </div>
  );
};

export const JAVASCRIPT_UNITIPLE = () => {
  return <div>用于指定脚本路径。</div>;
};

export const TRR_UNITIPLE = () => {
  return (
    <div>
      <div>用于配置树形功能的字段映射：</div>
      <div>id: 指定树形节点的id字段。</div>
      <div>text：指定树形节点的文本字段。</div>
      <div>code：指定树形节点的编码字段。</div>
      <div>nodeType：指定树形节点类型字段。</div>
      <div>moreRoot：指定是否是多根树。</div>
      <div>treeOrderIndex：指定树形节点排序字段。</div>
    </div>
  );
};

export const VIEW_UNITIPLE = () => {
  return (
    <div>
      <div>视图操作表信息是指视图功能有增删改操作时，同步操作原表业务数据。操作说明如下：</div>
      <div style="text-indent:2em;text-align:justify">
        <div>
          1、维护操作表信息时，仅需维护需要操作数据的表。若操作表之间存在主子关系时，并且主子表都需要新增数据，请将主表顺序放在子表上方。
        </div>
        <div>
          2、视图中的表需要操作的字段，需要在 “包含字段”
          中进行勾选字段。注意：当视图输出字段发生变化时，若需要进行增删改操作，则需要在包含字段中同步勾选。
        </div>
        <div>
          3、若需要字段之间赋值业务，则使用特殊字段带值配置。书写格式为：本视图字段1~本视图字段2,目标表字段1~目标表字段2。（说明：以逗号为分割，逗号前面为本视图字段，逗号后面为目标表字段，若多个字段则用~进行连接，前后位置对应即可。
        </div>
        <div>4、表数据需要操作时，根据业务需要进行勾选需更新、需添加、需删除。</div>
        <br />
        <div>操作表信息具体列说明如下：</div>
        <div>1、表编码：操作的原表表编码。</div>
        <div>2、表名称：操作的原表表名称。</div>
        <div>3、主键名：原表主键。</div>
        <div>4、主键对应本视图字段：原表主键编码对应的视图中的字段编码。</div>
        <div>5、包含字段：操作原表的字段范围。</div>
        <div>6、特殊字段带值配置：字段之间的赋值配置。</div>
        <div>7、需更新、需添加、需删除：对原表需要的操作，进行勾选。</div>
      </div>
    </div>
  );
};

export const CHILD_HEIGHT_UNITIPLE = () => {
  return <div>用于设置子功能面板高度。</div>;
};

export const CHILD_LOAD_UNITPLE = () => {
  return (
    <div>
      <div>用于设置子功能是否同主功能同步渲染，用户可以选择的方式有：</div>
      <div>
        按需加载（懒汉式）：主功能加载时，不加载子功能配置。当第一次打开子功能的时候会加载子功能配置。
      </div>
      <div>同时加载（饿汉式）：与主功能同时加载。</div>
    </div>
  );
};

export const FILTER_UNITIPLE = () => {
  const parm = '{@USER_CODE@}';
  const parm1 = '{@USER_NAME@}';
  const parm4 = '{@系统变量名@}';
  return (
    <div>
      <div>用于功能加载数据的过滤，基于功能最基础的条件追加其他附加过滤条件。支持如下方式: </div>
      <br />
      <div>1、多个条件时可使用and，or等进行连接查询</div>
      <div> 例: 筛选成年用户: and user_age &gt; 18 </div>
      <br />
      <div>2、过滤条件中引用系统公有变量</div>
      <div> 例: 筛选当前登录人录入的数据: </div>
      <div style="text-indent:1.5em">and create_usercode = {parm}</div>
      <br />
      <div>系统公有变量说明: </div>
      <div>
        ①、系统提供大量的公有变量，例如：当前登录人编码{parm}、当前登录人姓名{parm1}
        等，具体信息可查看平台API-平台默认提供的变量;
      </div>
      <div> ②、其他的系统变量,用 {parm4}即可获得。</div>
    </div>
  );
};

export const BUCKET_UNITIPLE = () => {
  return (
    <div>
      <div>存储桶是用于抽象文件存储的基础单元，用户可以把此存储桶理解为一个基础存储目录。</div>
      <div>
        用户可以在文档模块中创建存储桶。当功能指定为此存储桶时，依赖此功能的所有附件字段会把上传文件保存在此存储桶中。当没有创建存储桶时，则使用平台默认的存储桶。
      </div>
    </div>
  );
};

export const LEAVE_MARK_UNITPLE = () => {
  return (
    <div>
      当前功能对应的资源表如果应用多个功能，若您需要查看其他功能的数据留痕信息，请在此处配置该功能。
    </div>
  );
};

export const EMAIL_UNITPLE = () => {
  const code = '{名称字段编码}';
  const example = '{HTMC}';
  return (
    <div>
      <div>
        配置微邮内容中，关联业务数据的名称：格式为{code}。例如：{example}
      </div>
      <div>
        微邮配置方式：在功能列表中添加协作列之后，会在列表列中展示协作，点击【协作】会在右边展示微邮功能。
      </div>
    </div>
  );
};

export const ANNOTATION_UNITPLE = () => {
  const param = '{bean.HTMC}';
  return (
    <div>
      <div>
        配置批注的标题展示格式。批注标题将展示在系统首页的批注模块。动态变量可通过bean.字段编码进行获得。例如：订单合同：
        {param}，有新的批注消息，请及时查看！
      </div>
      <div>
        批注配置方式：在功能列表中添加协作列之后，会在列表列中展示协作，点击【协作】会在右边展示批注功能。
      </div>
    </div>
  );
};

export const CUSTOM_UNITPLE = () => {
  return (
    <div>
      功能列表中，列头右侧个性化列定义（齿轮图标）的开关。开启后则显示操作图标，点击后可以维护个性化列定义。
    </div>
  );
};

export const GRID_UNITPLE = () => {
  return <div>用于指定列表行内信息展示模板。</div>;
};

export const INPUT_UNITPLE = () => {
  return <div>用于提示用户该字段输入数据的要求，以灰色文字在输入框中展示。</div>;
};

export const INPUT_SUFFIX = () => {
  return <div>当背景色配置多个色值，以第一个色值为准。</div>;
};

export const OPEN_SEARCH_UNITIPLE = () => {
  return (
    <div>
      开启检索按钮后，在树形功能左侧数据结构下方，将显示检索按钮，点击后，可以跟随左侧选中的节点，右侧表单同步展示节点数据。若不开启，则左侧下方不显示检索按钮。
    </div>
  );
};

export const FORM_FILTER_UNITPLE = () => {
  return (
    <div>
      在查询选择等需要过滤条件的字段时使用，用于指定用户在弹出的查询选择列表中过滤符合条件的数据。
    </div>
  );
};

export const OERDER_UNITPLE = () => {
  return (
    <div>
      排序条件用于本功能的数据排序，系统提供两种快捷排序方式。您也可以根据实际业务场景调整排序列。
    </div>
  );
};

export const FORMMINWIDTH_UNITIPLE = () => {
  return <div>当表单宽度设置的数值为百分比%时，表单最小宽度才生效。</div>;
};

export const JS_ACTION_UNITPLE = () => {
  const params = '{style:{}}';
  return (
    <div>
      <div>1. 事件传入参数</div>
      <div style="padding-left: 17px">编辑器内，暴露了可以直接在代码中使用的全局变量如下：</div>
      <br />
      <div style="padding-left: 17px">
        - EventOptions：事件参数，包含了当前事件中的所有参数 <br />
        <div style="padding-left: 20px">
          const $func = EventOptions.$func; // 功能对象，所有事件都包含 ... 其他事件参数
        </div>
      </div>
      <br />
      <div style="padding-left: 17px">
        - JE: 公共类库，提供了常用的类库 <br />
        <div style="padding-left: 20px">const vue = JE.useVue(); // Vue库</div>
        <div style="padding-left: 20px">const ui = JE.useUi(); // UI库</div>
        <div style="padding-left: 20px"> const utils = JE.useUtils(); // 工具库 </div>
        <div style="padding-left: 20px">const system = JE.useSystem(); // 系统类库</div>
      </div>
      <br />
      <div> 2. 事件返回参数</div>
      <div style="padding-left: 17px">编辑器内，可以直接使用return关键字，进行返回参数：</div>
      <br />
      <div style="padding-left: 17px">- 普通出参：直接 return ... 即可 return true;</div>
      <br />
      <div style="padding-left: 17px">
        - 异步出参：return Promise;
        <br />
        <div style="padding-left: 20px">const utils = JE.useUtils(); // 工具库</div>
        <div style="padding-left: 20px">
          {' '}
          const deferred = utils.createDeferred(); // Promise异步队列函数
        </div>
        <div style="padding-left: 20px">// 业务逻辑执行完毕，可以调用 deferred.resolve();</div>
        <div style="padding-left: 20px">// 业务逻辑执行失败，可以调用 deferred.reject();</div>
        <div style="padding-left: 20px">return deferred.promise;</div>
        <br />
        <div style="padding-left: 17px">
          - 视图出参：必须返回 VNode，否则无法正确显示
          <br />
          <div style="padding-left: 10px">return JE.useVue().h('div',{params},'html内容');</div>
        </div>
      </div>
    </div>
  );
};
export const FORM_FILTER_REDIOCHECKBOX_CONFIG = () => {
  return (
    <div>
      <p>
        快查字段：点击左侧快速查询节点时，默认根据当前字段进行过滤数据。若您需要调整数据字段，可以在此项中填写您所需过滤的字段编码。
      </p>
      <p>
        快查取值字段：点击左侧查询节点时，默认取选中节点的CODE值。若您需要获取节点其他属性值，可以在此项中选择您所需的节点属性，即可获取该属性的值。
      </p>
      <p>
        注意：快查字段和快查取值字段两个配置项为配合使用，例如：您想在列表中过滤某个类型字段的NAME值，则您需要将快查字段配置为NAME，快查取值字段也需要选择对应NAME的属性，需要将两项的配置信息保持一致。
      </p>
    </div>
  );
};

export const ONLY_UNITPLE = () => {
  return (
    <div>
      唯一是指带值配置中带值规则左侧列表中的唯一。作用是用于查询选择时，右侧回显已选数据使用。
    </div>
  );
};

export const FUNCINFO_CLOSE_DRAG_SORT_UNITPLE = () => {
  return <div>关闭拖拽排序后，功能左侧树形不支持拖拽排序操作。</div>;
};

export const TRUNCATION_NUMBER_UNITPLE = () => {
  return (
    <div>
      <div>输入格式为：n,m</div>
      <div>n：从第几位开始截取，起始位置是0。</div>
      <div>m：从起始位置截取位数，其中*截取到最后字符。</div>
      <div>注：为空则显示全部内容</div>
      <br />
      <div>例如：日期格式是Y年m月d日 H时i分s秒 2023年05月31日 14时38分50秒</div>
      <div>0,11 &nbsp;&nbsp;// 2023年05月31日 </div>
      <div>5,6 &nbsp;&nbsp;// 05月31日 </div>
      <div>5,* &nbsp;&nbsp;// 05月31日 14时38分50秒 </div>
    </div>
  );
};

export const FUNCRELATION_TABLENAME_UNITPLE = () => {
  return (
    <div>
      <div>普通功能：</div>
      <div>① 可输入表编码。</div>
      <br />
      <div>附件类型：</div>
      <div>① 可输入表单中的附件字段，支持多个附件字段（用“,”隔开）。</div>
      <div>② 可输入service接口，例如：/je/demo/getFileData,返回的数据是JSONTreeNode。</div>
      <br />
      <div>微应用：</div>
      <div>① 可输入微应用编码。</div>
    </div>
  );
};
export const FUNCRELATION_CHILD_FIELD_UNITPLE = () => {
  return (
    <div>
      <div>1、功能配置中主从展示子功能展示方式为【表单(内部)分组展示】时，其他子功能布局失效；</div>
      <div>2、启用【表单(内部)分组展示】，子功能配置中的【分组】规则生效，规则如下：</div>
      <br />
      <div> * 规则1：组名1>组名11>组名111，普通展示，所有的子功能会挂接到对应的分组下面。</div>
      <div>
        *
        规则2：组名1>组名11>组名111*，最后一级带*号，表示下方不会挂接子功能，点击后所有子功能将会平铺展示）。
      </div>
      <div> * 规则3：规则中的组名可以自定义。</div>
    </div>
  );
};

export const FUNCRELATION_ZCGX_UNITPLE = () => {
  return (
    <div>
      <div>1、主从关系为主功能与子功能之间的数据对应关系。</div>
      <div>2、 主从关系为一对多时，子功能以 “功能列表” 的形式展示出一对多的数据。</div>
      <div>3、主从关系为一对一时，子功能以 “功能表单” 的形式展示出一对一的数据。</div>
    </div>
  );
};

export const LIST_PAGE_UNITIPLE = () => {
  return <div>如果设置-1，表示查询全部数据。</div>;
};
