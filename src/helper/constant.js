/**
 * 系统常量配置文件，命名规则
 * 平台核心：JE_模块_变量名
 * 业务常量：业务(MENU)_模块_变量名
 */

/**
 * 点击子系统入口页面的添加，下拉弹窗展示的数据
 */
import {
  ACTION_UNITIPLE,
  LIST_PAGE_UNITIPLE,
  JAVASCRIPT_UNITIPLE,
  TRR_UNITIPLE,
  VIEW_UNITIPLE,
  CHILD_HEIGHT_UNITIPLE,
  CHILD_LOAD_UNITPLE,
  FILTER_UNITIPLE,
  BUCKET_UNITIPLE,
  LEAVE_MARK_UNITPLE,
  EMAIL_UNITPLE,
  ANNOTATION_UNITPLE,
  CUSTOM_UNITPLE,
  GRID_UNITPLE,
  OERDER_UNITPLE,
  OPEN_SEARCH_UNITIPLE,
  FORMMINWIDTH_UNITIPLE,
  FUNCINFO_CLOSE_DRAG_SORT_UNITPLE,
} from './tip-constant.jsx';
import { getDDItemList } from '@jecloud/utils';
import srcimg0 from '@/components/function-base/img/child/0.png';
import srcimg1 from '@/components/function-base/img/child/1.png';
import srcimg2 from '@/components/function-base/img/child/2.png';
import srcimg3 from '@/components/function-base/img/child/3.png';
import srcimg4 from '@/components/function-base/img/child/4.png';
import srcimg5 from '@/components/function-base/img/child/5.png';

import dataimg3 from '@/components/function-base/img/data/FIRSTADD.png';
import dataimg1 from '@/components/function-base/img/data/FORM.png';
import dataimg2 from '@/components/function-base/img/data/FORMWIN.png';
import dataimg0 from '@/components/function-base/img/data/LIST.png';
import dataimg4 from '@/components/function-base/img/data/FORMDRAWER.png';
// 处理子功能显示位置的数据
export const getChildShowData = () => {
  const addKeysData = {
    formOuterHorizontal: {
      // label: '表单(外部)横向显示',
      unitTpl: '进入表单界面以横向Tab的形式展示',
      img: srcimg0,
    },
    formInnerHorizontal: {
      // label: '表单(内部)横向显示',
      unitTpl: '进入表单界面以横向Tab的形式展示',
      img: srcimg1,
    },
    formInnerVertical: {
      // label: '表单(内部)纵向显示',
      unitTpl: '进入表单界面以纵向摆放的形式展示',
      img: srcimg2,
    },
    formInnerGroup: {
      // label: '表单(内部)树形显示',
      unitTpl: '进入表单界面以树形的形式展示',
      img: srcimg3,
    },
    gridInnerHorizontal: {
      // label: '表格(内部)横向显示',
      unitTpl: '表格界面内部以横向Tab的形式展示',
      img: srcimg4,
    },
    gridOuterHorizontal: {
      // label: '表格(外部)并排显示',
      unitTpl: '子功能与主功能并排展示',
      img: srcimg5,
    },
  };
  const data = getDDItemList('JE_FUNC_CHILD_LAYOUT');
  const newData = data
    .filter((_item) => _item.code != 'parentFuncRow')
    .map((item) => {
      const { code, text } = item;
      item.value = code;
      item.label = text;
      delete item.code;
      delete item.text;
      Object.assign(item, { ...addKeysData[item.value] });
      return item;
    });
  return newData;
};

export const getInsertData = () => {
  const insertData = {
    FORM: {
      unitTpl: '进入表单界面维护数据。',
      img: dataimg1,
    },
    LIST: {
      unitTpl: '通过表格顶部控件维护数据库。',
      img: dataimg0,
    },
    FORMWIN: {
      unitTpl: '弹出表单界面维护数据。',
      img: dataimg2,
    },
    FIRSTADD: {
      unitTpl: '表格在首行构建录入控件维护数据。',
      img: dataimg3,
    },
    FORMDRAWER: {
      unitTpl: '右侧滑出表单界面维护数据。',
      img: dataimg4,
    },
  };
  const data = getDDItemList('JE_FUNC_INSERTTYPE');
  data.map((item) => {
    const { code, text } = item;
    item.value = code;
    item.label = text;
    delete item.code;
    delete item.text;
    Object.assign(item, { ...insertData[item.value] });
    return item;
  });
  return data;
};

export const ADD_FUNCTION_TYPE = [
  {
    value: 'SYSTEM',
    label: '子系统',
    disabled: false,
    compontent: 'ModalSystem',
    icon: 'jeicon jeicon-subsystem',
    color: '#3265F5',
  },
  {
    value: 'MODEL',
    label: '模块',
    disabled: false,
    compontent: 'AddModal',
    icon: 'jeicon jeicon-function-module',
    color: '#B8741A',
  },
  {
    value: 'FUNC',
    label: '功能',
    disabled: false,
    compontent: 'ModalAdd',
    icon: 'jeicon jeicon-function',
    color: '#606266',
  },
  {
    value: 'FUNCFIELD',
    label: '集合',
    disabled: false,
    compontent: 'ModalAdd',
    icon: 'jeicon jeicon-subfunction',
    color: '#606266',
  },
];

// 子系统中间的table的头部数据columns
export const FUNCTION_TABLE_COLUMNS = [
  {
    title: '名称',
    dataIndex: 'text',
    key: 'text',
    width: '300',
  },
  {
    title: '编码',
    dataIndex: 'code',
    key: 'code',
    width: '260',
  },
  {
    title: '类型',
    dataIndex: 'nodeInfoType',
    key: 'nodeInfoType',
    width: '140',
  },
  {
    title: '操作',
    dataIndex: 'operate',
    key: 'operate',
    width: '280',
  },
  {
    title: '功能描述',
    dataIndex: 'FUNCINFO_FUNCREMARK',
    key: 'FUNCINFO_FUNCREMARK',
  },
  {
    title: '创建人',
    dataIndex: 'SY_CREATEUSERNAME',
    key: 'SY_CREATEUSERNAME',
    width: '100',
  },
  {
    title: '创建时间',
    dataIndex: 'SY_CREATETIME',
    key: 'SY_CREATETIME',
    width: '200',
  },
  {
    title: '修改人',
    dataIndex: 'SY_MODIFYUSERNAME',
    key: 'SY_MODIFYUSERNAME',
    width: '100',
  },
  {
    title: '修改时间',
    dataIndex: 'SY_MODIFYTIME',
    key: 'SY_MODIFYTIME',
    width: '200',
  },
];

// 子系统添加功能之后类型选择
export const ADD_MODLE_FUNCTION_TYPE = [
  {
    value: 'PT',
    label: '普通功能',
    text: '普通功能',
    compontent: 'FunctionAdd',
  },
  {
    label: '树形功能',
    value: 'TREE',
    text: '树形功能',
    compontent: 'FunctionAdd',
  },
  {
    label: '操作视图',
    value: 'VIEW',
    text: '操作视图',
    compontent: 'FunctionAdd',
  },
];

export const OPERATE_BUTTON = [
  {
    value: 'update',
    label: '编辑',
    title: '编辑',
    compontent: 'ModalUpdate',
    disable: false,
    color: '#3265F5',
  },
  {
    value: 'copy',
    label: '复制',
    title: '复制',
    compontent: 'ModalCopy',
    disable: false,
    color: '',
    tip: '仅有【功能】允许复制',
  },
  {
    value: 'delete',
    label: '删除',
    title: '删除',
    compontent: 'ModalDelete',
    disable: false,
    color: '#A30014',
    tip: '存在下级内容，则不允许删除',
  },
  {
    value: 'refresh',
    label: '刷新',
    title: '刷新',
    compontent: 'ModalRefresh',
    disable: false,
    color: '',
    tip: '仅有【功能】允许刷新',
  },
  // hidden
  // {
  //   value: 'export',
  //   label: '导出',
  //   title: '导出',
  //   compontent: 'ModalExport',
  //   disable: false,
  //   color: '#0A7900',
  // },
];

// 子系统添加功能的时候，表名列表
export const FUNCTION_TABLE_MODAL_COLUMNS = [
  {
    title: 'No.',
    dataIndex: 'SY_ORDERINDEX',
    key: 'SY_ORDERINDEX',
  },
  {
    title: '表名称',
    dataIndex: 'RESOURCETABLE_TABLENAME',
    key: 'RESOURCETABLE_TABLENAME',
  },
  {
    title: '表编码',
    dataIndex: 'RESOURCETABLE_TABLECODE',
    key: 'RESOURCETABLE_TABLECODE',
  },
  {
    title: '主键编码',
    dataIndex: 'RESOURCETABLE_PKCODE',
    key: 'RESOURCETABLE_PKCODE',
  },
  {
    title: '已创建',
    dataIndex: 'RESOURCETABLE_ISCREATE',
    key: 'RESOURCETABLE_ISCREATE',
  },
  {
    title: '创建人',
    dataIndex: 'SY_CREATEUSERNAME',
    key: 'SY_CREATEUSERNAME',
  },
  {
    title: '创建时间',
    dataIndex: 'SY_CREATETIME',
    key: 'SY_CREATETIME',
  },
  {
    title: '注释',
    dataIndex: 'RESOURCETABLE_TABLENOTE',
    key: 'RESOURCETABLE_TABLENOTE',
  },
];

// 双击点击table的item进入功能配置页面，头部tab(功能)的最底部的tabs数据
export const FUNCTION_CONFIG_OPERATE = [
  {
    value: 'functionBase',
    label: '基础配置',
    title: '基础配置',
    compontent: 'FunctionBase',
  },
  // {
  //   value: 'dataMove',
  //   label: '数据流转策略',
  //   title: '数据流转策略',
  //   compontent: 'DataMove',
  // },
  // {
  //   value: 'pluginAdd',
  //   label: '插件业务增强',
  //   title: '插件业务增强',
  //   compontent: 'PluginAdd',
  // },
  {
    value: 'useInstruction',
    label: '使用说明',
    title: '使用说明',
    compontent: 'UseInstruction',
  },
  // {
  //   value: 'functionHelp',
  //   label: '帮助中心',
  //   title: '帮助中心',
  //   compontent: 'FunctionHelp',
  // },
];

// 双击点击table的item进入功能配置页面，头部tab(列表)的最底部的tabs数据
export const LIST_CONFIG_OPERATE = [
  {
    value: 'listBaseConfig',
    label: '基础配置',
    title: '基础配置',
    compontent: 'ListBaseConfig',
  },
  {
    value: 'listFieldConfig',
    label: '字段配置',
    title: '字段配置',
    compontent: 'ListFieldConfig',
  },
  // {
  //   value: 'listItemCunstom',
  //   label: '自定义列表',
  //   title: '自定义列表',
  //   compontent: 'ListItemCunstom',
  // },
  // {
  //   value: 'listPrint',
  //   label: '打印规划',
  //   title: '打印规划',
  //   compontent: 'ListPrint',
  // },
  // {
  //   value: 'functionHelp',
  //   label: '帮助中心',
  //   title: '帮助中心',
  //   compontent: 'FunctionHelp',
  // },
];

//  双击点击table的item进入功能配置页面，头部tab(表单)的最底部的tabs数据
export const FORM__CONFIG_OPERATE = [
  {
    value: 'formBaseConfig',
    label: '基础配置',
    title: '基础配置',
    compontent: 'FormBaseConfig',
  },
  {
    value: 'formFieldConfig',
    label: '字段配置',
    title: '字段配置',
    compontent: 'formFieldConfig',
  },
  {
    value: 'formDesign',
    label: '界面设计',
    title: '界面设计',
    compontent: 'FormDesign',
  },
  // {
  //   value: 'formPrint',
  //   label: '打印规划',
  //   title: '打印规划',
  //   compontent: 'FormPrint',
  // },
  // {
  //   value: 'functionHelp',
  //   label: '帮助中心',
  //   title: '帮助中心',
  //   compontent: 'FunctionHelp',
  // },
];

// 功能配置下的tabs
export const FUNCTION_TABS_DATA = [
  {
    value: 'function',
    label: '功能',
    title: '功能',
    icon: 'jeicon jeicon-function',
    compontent: 'Function',
    childTabData: FUNCTION_CONFIG_OPERATE,
  },
  {
    value: 'list',
    label: '列表',
    title: '列表',
    compontent: 'List',
    icon: 'fal fa-th-list',
    childTabData: LIST_CONFIG_OPERATE,
  },
  {
    value: 'form',
    label: '表单',
    title: '表单',
    compontent: 'Form',
    icon: 'jeicon jeicon-from',
    childTabData: FORM__CONFIG_OPERATE,
  },
  {
    value: 'button',
    label: '按钮',
    title: '按钮',
    icon: 'fal fa-mouse',
    compontent: 'Button',
    childTabData: [],
  },
  {
    value: 'functionChild',
    label: '子功能',
    title: '子功能',
    icon: 'jeicon jeicon-subfunction',
    compontent: 'FunctionChild',
    childTabData: [],
  },
];

//------start
// 功能配置中功能tabs签下的表单数据

// 功能配置的功能tab中核心配置
const CORE_CONFIGURATION = [
  {
    type: 'input',
    label: '功能编码',
    require: true,
    value: 'FUNCINFO_FUNCCODE',
    layout: 'flex',
    disabled: true,
    suffixcontent: '编辑',
  },
  {
    type: 'input',
    label: '功能名称',
    require: true,
    value: 'FUNCINFO_FUNCNAME',
    layout: 'flex',
  },
  {
    type: 'input',
    label: 'Action',
    require: false,
    unitTpl: ACTION_UNITIPLE,
    value: 'FUNCINFO_FUNCACTION',
    layout: 'flex',
  },
  {
    type: 'input-select',
    label: '表名',
    require: true,
    value: 'FUNCINFO_TABLENAME',
    layout: 'flex',
    disabled: false,
    configInfo:
      'JE_CORE_RESOURCETABLE,FUNCINFO_TABLENAME~FUNCINFO_PKNAME,RESOURCETABLE_TABLECODE~RESOURCETABLE_PKCODE,S',
    orders: [{ code: 'SY_ORDERINDEX', type: 'ASC' }],
  },
  {
    type: 'input',
    label: '主键名称',
    require: true,
    value: 'FUNCINFO_PKNAME',
    layout: 'flex',
    disabled: false,
  },
  {
    type: 'selectFunction',
    label: '数据录入方式',
    require: false,
    layout: 'flex',
    complete: true,
    selectData: [],
    value: 'FUNCINFO_INSERTTYPE',
  },
  {
    type: 'select',
    label: '功能类型',
    require: true,
    layout: 'flex',
    value: 'FUNCINFO_FUNCTYPE',
    configInfo: 'JE_FUNC_TYPE,FUNCINFO_FUNCTYPE,code,S',
  },
  // {
  //   type: 'select-grid',
  //   label: '操作表名',
  //   require: false,
  //   value: 'FUNCINFO_CRUDTABLENAME',
  //   layout: 'flex',
  //   showType: 'VIEW',
  //   configInfo: 'JE_CORE_RESOURCETABLE,FUNCINFO_CRUDTABLENAME,RESOURCETABLE_TABLECODE,S',
  //   order: [{ code: 'SY_ORDERINDEX', type: 'ASC' }],
  // },
  {
    type: 'select',
    label: '开启检索按钮',
    require: false,
    value: 'FUNCINFO_OPEN_SEARCH_BUTTON',
    layout: 'flex',
    showType: 'TREE',
    unitTpl: OPEN_SEARCH_UNITIPLE,
    selectData: [
      { value: '1', label: '是' },
      { value: '0', label: '否' },
    ],
  },
  {
    type: 'treeTable',
    label: '树形功能配置项',
    require: false,
    value: 'FUNCINFO_FUNCDICCONFIG',
    layout: 'block',
    showType: 'TREE',
    unitTpl: TRR_UNITIPLE,
    data: [
      {
        label: '属性',
        value: 'key',
        type: 'input',
        width: '220',
      },
      {
        label: '值',
        value: 'value',
        type: 'input',
        width: '220',
      },
      {
        label: '备注',
        value: 'remark',
        type: 'input',
      },
    ],
  },
  {
    type: 'select',
    label: ' 开启更多按钮',
    require: false,
    value: 'FUNCINFO_OPEN_MORE_BUTTON',
    layout: 'flex',
    showType: 'TREE',
    selectData: [
      { value: '1', label: '是' },
      { value: '0', label: '否' },
    ],
  },
  {
    type: 'select',
    label: '关闭拖拽排序',
    require: false,
    value: 'FUNCINFO_CLOSE_DRAG_SORT',
    layout: 'flex',
    showType: 'TREE',
    unitTpl: FUNCINFO_CLOSE_DRAG_SORT_UNITPLE,
    selectData: [
      { value: '1', label: '是' },
      { value: '0', label: '否' },
    ],
  },
  {
    type: 'select',
    label: ' 展示方式',
    require: false,
    value: 'FUNCINFO_SHOW_TYPE',
    layout: 'flex',
    showType: 'TREE',
    configInfo: 'JE_FUNC_TREE_SHOWTYPE,FUNCINFO_SHOW_TYPE,code,S',
  },
  {
    type: 'select',
    label: ' 是否懒加载',
    require: false,
    value: 'FUNCINFO_SFLJZ',
    layout: 'flex',
    showType: 'TREE',
    selectData: [
      { value: '1', label: '是' },
      { value: '0', label: '否' },
    ],
  },
  {
    type: 'treeTable1',
    label: ' 更多按钮配置',
    require: false,
    value: 'FUNCINFO_MORE_BUTTON_CONFIG',
    showType: 'TREE',
    data: [
      {
        label: '按钮名',
        value: 'buttonName',
        type: 'input',
        width: '140',
      },
      {
        label: '图标',
        value: 'icon',
        type: 'input',
        width: '220',
      },
      {
        label: '方法',
        value: 'function',
        type: 'input',
        width: '',
      },
      {
        label: '显隐表达式',
        value: 'expression',
        type: 'input',
        width: '280',
      },
      {
        label: '类型',
        value: 'type',
        type: 'input',
        width: '100',
      },
      {
        label: '类型依赖按钮',
        value: 'rely',
        type: 'input-select',
        width: '140',
      },
    ],
  },

  {
    type: 'treeTable2',
    label: '视图操作表信息',
    require: false,
    value: 'FUNCINFO_VIEWCONFIGINFO',
    layout: 'block',
    unitTpl: VIEW_UNITIPLE,
    showType: 'VIEW',
    data: [
      {
        label: '表名',
        value: 'name',
        type: 'input',
        width: '140',
      },
      {
        label: '主键字段名',
        value: 'majorname',
        type: 'input',
      },
      {
        label: '主键对应视图字段',
        value: 'view',
        type: 'input',
        width: '160',
      },
      {
        label: '包含字段',
        value: 'contain',
        type: 'input',
        width: '140',
      },
      {
        label: '特殊字段带值配置',
        value: 'special',
        type: 'input',
        width: '160',
      },
      {
        label: '需更新',
        value: 'update',
        type: 'checkbox',
        width: '80',
      },
      {
        label: '需添加',
        value: 'add',
        type: 'checkbox',
        width: '80',
      },
      {
        label: '需删除',
        value: 'delete',
        type: 'checkbox',
        width: '80',
      },
    ],
  },
  // {
  //   type: 'input',
  //   label: '存储过程',
  //   require: true,
  //   value: 'FUNCINFO_PROCEDURE',
  //   layout: 'flex',
  //   showType: 'procedure',
  // },
  // {
  //   type: 'select',
  //   label: '三方数据源',
  //   require: false,
  //   value: 'FUNCINFO_DBNAME',
  //   layout: 'flex',
  //   showType: 'procedure',
  //   selectData: [],
  // },
  // {
  //   type: 'select',
  //   label: '三方数据源',
  //   require: false,
  //   value: 'FUNCINFO_DBNAME',
  //   layout: 'flex',
  //   showType: 'sql',
  //   selectData: [
  //     { value: '0', label: '否' },
  //     { value: '1', label: '是' },
  //   ],
  // },
  // {
  //   type: 'textarea',
  //   label: 'SQL',
  //   require: true,
  //   value: 'FUNCINFO_SQL',
  //   layout: 'block',
  //   showType: 'sql',
  // },
  // {
  //   type: 'treeTable3',
  //   label: '查询参数',
  //   require: false,
  //   value: 'FUNCINFO_QUERYPARAM',
  //   layout: 'block',
  //   showType: 'procedure',
  //   data: [
  //     {
  //       label: '参数',
  //       value: 'name',
  //       type: 'input',
  //       width: '220',
  //     },
  //     {
  //       label: '参数类型',
  //       value: 'fieldType',
  //       type: 'input', // todo
  //       width: '220',
  //     },
  //     {
  //       label: '主键对应视图字段',
  //       value: 'paramType',
  //       type: 'checkbox',
  //       width: '220',
  //     },
  //     {
  //       label: '包含字段',
  //       value: 'defaultValue',
  //       type: 'input',
  //     },
  //   ],
  // },
  // {
  //   type: 'treeTable4',
  //   label: '查询参数',
  //   require: false,
  //   value: 'FUNCINFO_QUERYPARAM_SQL',
  //   layout: 'block',
  //   showType: 'sql',
  //   data: [
  //     {
  //       label: '参数',
  //       value: 'name',
  //       type: 'input',
  //       width: '220',
  //     },
  //     {
  //       label: '参数类型',
  //       value: 'fieldType',
  //       type: 'input', // todo
  //       width: '220',
  //     },
  //     {
  //       label: '主键对应视图字段',
  //       value: 'paramType',
  //       type: 'checkbox',
  //       width: '220',
  //     },
  //     {
  //       label: '包含字段',
  //       value: 'defaultValue',
  //       type: 'input',
  //     },
  //   ],
  // },
];

const MASTER_SLAVE = [
  {
    type: 'selectFunction',
    label: '子功能展示方式',
    require: false,
    // unitTpl: 'special',
    complete: true,
    value: 'FUNCINFO_CHILDSHOWTYPE',
    layout: 'flex',
    selectData: [],
  },
  // {
  //   type: 'select',
  //   label: '子功能加载方式', //字段值TODO
  //   require: false,
  //   unitTpl: CHILD_LOAD_UNITPLE,
  //   value: 'FUNCINFO_CHILDREFRESH',
  //   layout: 'flex',
  //   selectData: [
  //     { value: '0', label: '按需加载' },
  //     { value: '1', label: '初始化主功能时同时加载' },
  //   ],
  // },
  {
    type: 'table',
    label: '子功能高度',
    require: false,
    complete: true,
    unitTpl: CHILD_HEIGHT_UNITIPLE,
    value: 'FUNCINFO_CHILDHEIGHT',
    layout: 'flex',
    selectData: [
      { value: 'default', label: '通用' },
      { value: '1280x1024', label: '1280x1024' },
      { value: '1440x900', label: '1440x900' },
      { value: '1600x1200', label: '1600x1200' },
      { value: '1680x1050', label: '1680x1050' },
      { value: '1920x1080', label: '1920x1080' },
      { value: '2048x1536', label: '2048x1536' },
    ],
  },
  // {
  //   type: 'switch',
  //   label: '一对一表单',
  //   require: false,
  //   value: 'FUNCINFO_ONETOFORM',
  //   layout: 'block',
  //   complete: true,
  //   describe:
  //     '主功能下的所有子功能最多且只有一条数据，配置此选项的功能的所有子功能将无法展示出列表页面',
  // },
  // {
  //   type: 'switch',
  //   label: '列表子功能平级显示',
  //   require: false,
  //   complete: true,
  //   value: 'FUNCINFO_GRIDCHILDSS',
  //   layout: 'block',
  //   describe:
  //     '子功能与列表主功能上下平级显示,取消则在列表下显示。【只有列表子功能起效，有快速查询可看效果】',
  // },
  // {
  //   type: 'switch',
  //   label: '多级子功能导航',
  //   require: false,
  //   value: 'FUNCINFO_CHILDFUNC_NAVIGATION',
  //   layout: 'block',
  //   complete: true,
  //   describe:
  //     '启动该项子功能首行为子功能面包屑导航，关闭则按多级子功能平铺展示。【只有展示方式为：“表单(外部)横向展示”时起效】',
  // },
];
const WORKFLOW = [
  {
    type: 'workflow',
    label: '绑定工作流',
    require: false,
    layout: 'block',
    disabled: true,
    value: 'FUNCINFO_BINDING_WORKFLOW',
    selectData: [],
  },
  {
    type: 'switch',
    label: '启动工作流',
    require: false,
    layout: 'block',
    value: 'FUNCINFO_USEWF',
    describe:
      '启动该项功能就启动了工作流引擎【需要事先完成工作流到功能的挂接】。注：功能列表保存按钮将禁用，列表不可编辑。',
  },
  // {
  //   type: 'switch',
  //   label: '只读流程',
  //   require: false,
  //   layout: 'block',
  //   value: 'FUNCINFO_READONLYWF',
  //   describe: '只能查看流程跟踪与启动，不能进行审批操作。【适用一个表多功能的流程跟踪查看】',
  // },
];
const SEARCHCONFIG = [
  {
    type: 'textarea',
    label: '过滤条件（whereSql）',
    require: false,
    layout: 'block',
    placeholder: 'and...',
    unitTpl: FILTER_UNITIPLE,
    value: 'FUNCINFO_WHERESQL',
  },
  {
    type: 'textarea',
    label: '过滤条件描述',
    require: false,
    layout: 'block',
    value: 'FUNCINFO_WHERESQL_DES',
    operate: [
      { text: '自然语言智能翻译', code: 'FUNCINFO_WHERESQL', name: '过滤条件（whereSql）' },
    ],
  },
  {
    type: 'input',
    label: '排序条件（orderSql）',
    require: false,
    layout: 'block',
    placeholder: 'order by...',
    unitTpl: OERDER_UNITPLE,
    operate: [
      { text: '排序字段正序', code: 'sort', content: 'ORDER BY SY_ORDERINDEX ASC' },
      { text: '创建时间倒序', code: 'time', content: 'ORDER BY SY_CREATETIME DESC' },
    ],
    value: 'FUNCINFO_ORDERSQL',
  },
  {
    type: 'textarea',
    label: '排序条件描述',
    require: false,
    layout: 'block',
    value: 'FUNCINFO_ORDERSQL_DES',
    operate: [
      { text: '自然语言智能翻译', code: 'FUNCINFO_ORDERSQL', name: '排序条件（orderSql）' },
    ],
  },
  {
    type: 'input',
    label: '快速查询操作说明',
    require: false,
    layout: 'flex',
    value: 'FUNCINFO_TREETIP',
  },

  // {
  //   type: 'input',
  //   label: '其他语种',
  //   require: false,
  //   layout: 'flex',
  //   value: 'FUNCINFO_TREETIP_EN',
  // },
  {
    type: 'input',
    label: '快速查询标题',
    require: true,
    layout: 'flex',
    value: 'FUNCINFO_TREETITLE',
  },
  {
    type: 'inputNumber',
    label: '快速查询宽度',
    require: true,
    layout: 'flex',
    value: 'FUNCINFO_TREEWIDTH',
    suffix: 'px',
  },
  {
    type: 'switch',
    label: '快速查询分步加载',
    require: false,
    layout: 'block',
    value: 'FUNCINFO_TREEREFRESH',
    describe: '启用该项快速查询不会展开，只有展开时才会加载数据。',
  },
  {
    type: 'switch',
    label: '高级查询分步加载',
    require: false,
    layout: 'block',
    value: 'FUNCINFO_GROUPFORMOPEN',
    describe: '启用该项高级查询不会展开，只有展开时才会加载数据。',
  },
  // {
  //   type: 'switch',
  //   label: '高级查询禁用change事件',
  //   require: false,
  //   layout: 'block',
  //   value: 'FUNCINFO_DISABLE_CHANGE',
  //   describe: '禁用后只有单击查询按钮才会进行查询操作，不再监控每种查询组件的数值变化。',
  // },
  {
    type: 'checkbox',
    label: '禁用查询元素',
    require: false,
    layout: 'block',
    checkboxData: [
      { label: '高级查询', value: 'group' },
      { label: '查询策略', value: 'strategy' },
      // { label: '列头查询', value: 'column' },
      { label: '关键字查询', value: 'keyword' },
    ],
    value: 'FUNCINFO_DISABLEQUERYSQL',
    describe: '禁用后只有单击查询按钮才会进行查询操作，不再监控每种查询组件的数值变化。',
  },
];
const EXTENDEDFUNCTION = [
  {
    type: 'input',
    label: '附件Bucket',
    require: false,
    unitTpl: BUCKET_UNITIPLE,
    layout: 'block',
    value: 'FUNCINFO_ATTACHMENTPATH',
  },
  {
    type: 'switch',
    label: '启用审核功能',
    require: false,
    unitTpl: '',
    layout: 'block',
    value: 'FUNCINFO_ENABLEDSH',
    describe: '启动该项功能就启动了审核功能【审核、弃审按钮及对应的审核字段会自动添加】。',
  },
  // {
  //   type: 'switch',
  //   label: '启用数据留痕',
  //   require: false,
  //   complete: true,
  //   layout: 'block',
  //   value: 'FUNCINFO_USEDATALOG',
  //   describe:
  //     '启动该项功能将记录用户每次修改数据的痕迹，在对应的子功能中展示.【需配合表单字段的留痕选项】',
  // },
  // {
  //   type: 'input-select',
  //   label: '留痕依赖功能',
  //   complete: true,
  //   require: false,
  //   unitTpl: LEAVE_MARK_UNITPLE,
  //   placeholder: '填写功能编码，主要处理一个表多个功能要修改记录一致。',
  //   layout: 'block',
  //   value: 'FUNCINFO_LEAVE_MARK_FUNC',
  //   configInfo: 'JE_CORE_RESOURCETABLE,FUNCINFO_LEAVE_MARK_FUNC,RESOURCETABLE_TABLENAME,S',
  //   orders: [{ code: 'SY_ORDERINDEX', type: 'ASC' }],
  //   describe:
  //     '启动该项功能将记录用户每次修改数据的痕迹，在对应的子功能中展示.【需配配合表单字段的留痕选项】',
  // },
  {
    type: 'textarea',
    label: '功能描述',
    require: false,
    layout: 'block',
    value: 'FUNCINFO_FUNCREMARK',
    length: 100,
  },
  {
    type: 'input',
    label: '创建人',
    require: false,
    layout: 'flex',
    value: 'SY_CREATEUSERNAME',
    disabled: true,
  },
  {
    type: 'input',
    label: '创建时间',
    require: false,
    layout: 'flex',
    value: 'SY_CREATETIME',
    disabled: true,
  },
  {
    type: 'input',
    label: '修改人',
    require: false,
    layout: 'flex',
    value: 'SY_MODIFYUSERNAME',
    disabled: true,
  },
  {
    type: 'input',
    label: '修改时间',
    require: false,
    layout: 'flex',
    value: 'SY_MODIFYTIME',
    disabled: true,
  },
];
export const FUNCTION_BSAE_FORM = [
  {
    value: 'coreConfig',
    label: '核心配置',
    icon: 'jeicon jeicon-configure',
    formData: CORE_CONFIGURATION,
  },
  {
    value: 'masterSlave',
    label: '主从展示',
    icon: 'fas fa-tv',
    formData: MASTER_SLAVE,
  },
  {
    value: 'workflow',
    label: '流程绑定',
    icon: 'fas fa-bezier-curve',
    formData: WORKFLOW,
  },
  {
    value: 'serchConfig',
    label: '查询配置',
    icon: 'fas fa-search-plus',
    formData: SEARCHCONFIG,
  },
  {
    value: 'extendedFunction',
    label: '扩展功能',
    icon: 'fas fa-clipboard-list',
    formData: EXTENDEDFUNCTION,
  },
];

export const ASSOCIATION_DATA = [
  { code: 'no', text: '--------------' },
  { code: '=', text: '等于' },
  { code: '>', text: '大于' },
  { code: '<', text: '小于' },
  { code: '>=', text: '大于等于' },
  { code: '<=', text: '小于等于' },
  { code: '!=', text: '不等于' },
  { code: 'in ', text: '包含' },
  { code: 'notIn', text: '不包含' },
  { code: 'like', text: 'LIKE' },
  { code: 'idit', text: '自定义' },
];

export const CORE_FUNCTION = [
  { value: 'func', label: '普通功能' },
  // { value: 'idit', label: '自定义功能' },
  // { value: 'tree', label: '树形功能' },
  // { value: 'view', label: '操作视图' },
  // { value: 'chart', label: '统计图' },
  // { value: 'report', label: '报表' },
  { value: 'file', label: '附件' },
  // { value: 'industry', label: '工业图' },
  // { value: 'gantt', label: '甘特图' },
  // { value: 'portal', label: '首页Portal' },
  // { value: 'childfuncfield', label: '子功能集合' },
  // { value: 'procedure', label: '存储过程' },
  // { value: 'iditprocedure', label: '动态列存储过程' },
  // { value: 'sql', label: 'SQL' },
  // { value: 'history', label: '历史留痕' },
];

export const CHILDREN_LAYOUT = [
  { text: '表单(外部)横向显示', code: 'formOuterHorizontal' },
  { text: '表单(内部)横向显示', code: 'formInnerHorizontal' },
  { text: '表单(内部)纵向显示', code: 'formInnerVertical' },
  // { text: '表单(内部)树形显示', code: 'formInnerTree' },
  // { text: '表格(内部)横向显示', code: 'gridInnerHorizontal' },
  // { text: '表格(外部)并排显示', code: 'gridOuterHorizontal' },
];

export const DATAFLOW_DZ = [
  { code: 'gridInsertBtn', text: '添加【列表】' },
  { code: 'gridUpdateBtn', text: '保存【列表】' },
  { code: 'gridRemoveBtn', text: '删除【列表】' },
  { code: 'formSaveBtn', text: '保存【表单】' },
];

export const DATAFLOW_CZFS = [
  { code: 'more', text: '逐条操作' },
  { code: 'one', text: '全表操作' },
  { code: 'idit', text: '自定义' },
];

export const DATAFLOW_CZ = [
  { code: 'update', text: 'update' },
  { code: 'delete', text: 'delete' },
  { code: 'insert', text: 'insert' },
];

//------end

// 列表表单start
export const LIST_FORM_CONFIG = [
  {
    type: 'inputNumber',
    label: '每页展示条数',
    require: true,
    layout: 'flex',
    value: 'FUNCINFO_PAGESIZE',
    minValue: -1,
    unitTpl: LIST_PAGE_UNITIPLE,
  },
  {
    type: 'select',
    label: '默认行高',
    require: false,
    layout: 'flex',
    value: 'FUNCINFO_COLUMN_WIDTH',
    configInfo: 'JE_FUNC_ROWSIZE,FUNCINFO_COLUMN_WIDTH,code,S',
  },
  {
    type: 'input',
    label: '操作说明',
    require: false,
    layout: 'flex',
    defaultValue: '无数据时展示的提示性文字',
    value: 'FUNCINFO_GRIDTIP',
  },
  // {
  //   type: 'input',
  //   label: '分组字段',
  //   require: false,
  //   complete: true,
  //   layout: 'flex',
  //   defaultValue: '字段编码~ASC 或 字段编码~DESC',
  //   value: 'FUNCINFO_GROUPFIELD',
  // },
  // {
  //   type: 'input',
  //   label: '分组展示模板',
  //   require: false,
  //   complete: true,
  //   layout: 'flex',
  //   defaultValue: '字段名称：{name}({rows.lenght} 条)',
  //   value: 'FUNCINFO_GROUPFIELDTPL',
  // },
  {
    type: 'radio',
    label: '分页条位置信息',
    require: false,
    layout: 'flex',
    value: 'FUNCINFO_PAGEINFOALIGN',
    data: [
      { value: 'left', label: '左侧' },
      { value: 'right', label: '右侧' },
      { value: 'hidden', label: '隐藏' },
    ],
  },
  {
    type: 'checkbox',
    label: '隐藏表格线',
    require: false,
    layout: 'flex',
    value: 'FUNCINFO_GRIDHIDELINES',
    checkedKey: 'checkAll',
    data: [
      { value: 'bottom', label: '横线' },
      { value: 'right', label: '竖线' },
    ],
  },
  {
    type: 'checkbox',
    label: '统计策略',
    require: false,
    layout: 'flex',
    checkedKey: 'statistics',
    value: 'FUNCINFO_STATISTICALSTRATEGY',
    data: [
      { value: 'single', label: '单页统计' },
      { value: 'all', label: '全局统计' },
    ],
  },
  // {
  //   type: 'input',
  //   label: '批注标题',
  //   require: false,
  //   complete: true,
  //   unitTpl: ANNOTATION_UNITPLE,
  //   layout: 'flex',
  //   value: 'FUNCINFO_ANNOTATION_TITLE',
  // },
  // {
  //   type: 'input',
  //   label: '微邮标题',
  //   require: false,
  //   complete: true,
  //   unitTpl: EMAIL_UNITPLE,
  //   layout: 'flex',
  //   value: 'FUNCINFO_MICRO_MAIL_TITLE',
  // },
  // {
  //   type: 'switch',
  //   label: '单条进表单',
  //   require: false,
  //   complete: true,
  //   layout: 'block',
  //   remarks: '当列表有且只有一条数据时，平台会自动打开其对应的表单页面。',
  //   value: 'FUNCINFO_ONETOFORM',
  // },
  {
    type: 'switch',
    complete: true,
    label: '启用简洁按钮条',
    require: false,
    layout: 'block',
    remarks: '启用后操作按钮居左显示，搜索框居右显示。',
    value: 'FUNCINFO_SIMPLEBAR',
  },
  {
    type: 'switch',
    label: '允许多选',
    require: false,
    layout: 'block',
    remarks: '列表会出现多选框，用于批量选择。',
    value: 'FUNCINFO_MULTISELECT',
  },
  {
    type: 'switch',
    label: '拖动排序',
    require: false,
    layout: 'block',
    remarks: '列表分页将会去掉，数据可以进行拖动排序。',
    value: 'FUNCINFO_DDORDER',
  },
  // {
  //   type: 'switch',
  //   label: '多选过滤',
  //   require: false,
  //   complete: true,
  //   layout: 'block',
  //   remarks: '选择多条时，列表上的子功能会根据选中的多条数据进行过滤。',
  //   value: 'FUNCINFO_CHILDFILTER',
  // },
  {
    type: 'switch',
    label: '隐藏工具条',
    require: false,
    layout: 'block',
    remarks: '开启后按钮工具条将会被隐藏，重新打开可以选择子系统功能作为入口激活功能配置界面。',
    value: 'FUNCINFO_HIDEGRIDTBAR',
  },
  // {
  //   type: 'switch',
  //   label: '无限滚动',
  //   require: false,
  //   layout: 'block',
  //   complete: true,
  //   remarks: '开启无限滚动时，分页条将被隐藏。',
  //   value: 'FUNCINFO_GRIDBUFFERED',
  // },
  // {
  //   type: 'switch',
  //   label: '启用树形列表视图',
  //   require: false,
  //   complete: true,
  //   layout: 'block',
  //   remarks: '启用可切换视图，只有树形功能有效。',
  //   value: 'FUNCINFO_USETREEGRID',
  // },
  // {
  //   type: 'switch',
  //   label: '启用数据标记',
  //   require: false,
  //   complete: true,
  //   layout: 'block',
  //   remarks: '启用后在列表上自行添加标记列，便可对数据进行颜色标记分类了。',
  //   value: 'FUNCINFO_USEMARK',
  // },
  // {
  //   type: 'switch',
  //   label: '启用修改标记',
  //   require: false,
  //   layout: 'block',
  //   remarks: '启用后在列表上自行修改标记列，用于提示数据已经被修改了。',
  //   value: 'FUNCINFO_USEEDIT',
  // },
  {
    type: 'switch',
    label: '初始化加载列表数据',
    require: false,
    layout: 'block',
    remarks: '默认加载列表初始化数据，关闭后则不加载列表数据，需要点击查询再加载数据。',
    value: 'FUNCINFO_INIT_LOAD_DATA',
  },
  // {
  //   type: 'switch',
  //   label: '行综合展板默认打开',
  //   require: false,
  //   layout: 'block',
  //   remarks: '开启以后每一行默认全会打开综合展示面板。',
  //   value: 'FUNCINFO_GRIDROWTIPSHOW',
  //   complete: true,
  // },
  // {
  //   type: 'textarea',
  //   label: '行综合展板',
  //   require: false,
  //   complete: true,
  //   layout: 'block',
  //   value: 'FUNCINFO_GRIDROWTIP',
  //   unitTpl: GRID_UNITPLE,
  // },
  {
    type: 'inputNumber',
    label: '数据请求超时',
    require: false,
    layout: 'flex',
    value: 'FUNCINFO_GRIDTIMEOUT',
    suffix: '秒',
  },
  {
    type: 'select',
    label: '隔行变色',
    require: false,
    layout: 'flex',
    value: 'FUNCINFO_INTERLACED_DISCOLOUR',
    data: [
      { value: '1', label: '开启' },
      { value: '0', label: '关闭' },
    ],
  },
  {
    type: 'select',
    label: '行编辑',
    require: false,
    layout: 'flex',
    value: 'FUNCINFO_LINE_EDIT',
    data: [
      { value: '1', label: '开启' },
      { value: '0', label: '关闭' },
    ],
  },
  // {
  //   type: 'select',
  //   label: '个性化列定义',
  //   require: false,
  //   layout: 'flex',
  //   value: 'FUNCINFO_CUSTOM_COLUMN',
  //   data: [
  //     { value: '1', label: '开启' },
  //     { value: '0', label: '关闭' },
  //   ],
  //   unitTpl: CUSTOM_UNITPLE,
  //   complete: true,
  // },
];
// end

// 表单start
export const FORM_CONFIG = [
  {
    type: 'input',
    label: '表单标题',
    require: true,
    layout: 'flex',
    value: 'FUNCINFO_FORMTITLE4FUNC',
  },
  {
    type: 'select',
    label: '表单列数',
    require: true,
    layout: 'flex',
    configInfo: 'JE_FUNC_LAYOUT_COLS,FUNCINFO_FORMCOLS,code,S',
    value: 'FUNCINFO_FORMCOLS',
  },
  // {
  //   type: 'checkbox',
  //   label: '表单分页',
  //   complete: true,
  //   require: false,
  //   remarks: '勾选后需进一步配置分页信息规则。',
  //   layout: 'flex',
  //   value: 'FUNCINFO_FORMPAGING',
  //   text: '启用',
  //   data: [{ label: '启用', value: '1' }],
  // },
  // {
  //   type: 'input',
  //   label: '分页配置',
  //   complete: true,
  //   defaultValue: '{rownum}.{字段编码}~~width{列表宽度}',
  //   require: false,
  //   layout: 'flex',
  //   value: 'FUNCINFO_FORMPAGINGCONFIG',
  // },
  {
    type: 'inputNumber',
    label: '字段标题宽',
    require: false,
    layout: 'flex',
    suffixafter: 'px',
    value: 'FUNCINFO_FORMLABELWIDTH',
  },
  // {
  //   type: 'inputNumber',
  //   label: '其他语种字段标题宽',
  //   require: false,
  //   layout: 'flex',
  //   value: 'FUNCINFO_FORMLABELWIDTH_EN',
  // },
  {
    type: 'color',
    label: '字段标题颜色',
    require: false,
    layout: 'flex',
    defaultValue: '#fff',
    value: 'FUNCINFO_FORM_LABELCOLOR',
  },
  {
    type: 'inputNumber',
    label: '表单最小宽度',
    require: false,
    layout: 'flex',
    suffixafter: 'px',
    value: 'FUNCINFO_FORMMINWIDTH',
    unitTpl: FORMMINWIDTH_UNITIPLE,
  },
  {
    type: 'color',
    label: '表单背景色',
    require: false,
    defaultValue: '#fff',
    layout: 'flex',
    value: 'FUNCINFO_FORMBGCOLOR',
  },
  {
    type: 'input',
    label: '表单宽度',
    require: false,
    layout: 'flex',
    suffixafter: '支持数值px、百分比%',
    defaultValue: 1024,
    value: 'FUNCINFO_FORMWIDTH',
  },
  // {
  //   type: 'input',
  //   label: '打印表单标题',
  //   require: false,
  //   complete: true,
  //   layout: 'flex',
  //   value: 'FUNCINFO_PRINT_FORM_TITLE',
  // },
  {
    type: 'input',
    label: '弹出时表单宽高',
    require: false,
    layout: 'flex',
    defaultValue: '例：1024,768或100%,100%，表单侧滑时高度无效。',
    value: 'FUNCINFO_WINFORMWH',
    suffixafter: '支持数值px、百分比%',
  },
  {
    type: 'inputNumber',
    label: '字段左右内间距',
    require: false,
    layout: 'flex',
    suffixafter: 'px',
    value: 'FUNCINFO_FORM_FIELD_LEFTRIGHT_PADDING',
  },
  {
    type: 'radio',
    label: '表单字段间距',
    require: false,
    layout: 'block',
    value: 'FUNCINFO_FORMFIELD_SPACING',
    data: [
      { value: '0', label: '默认紧凑' },
      { value: '1', label: '默认标准' },
    ],
  },
  // {
  //   type: 'radio',
  //   label: '表单状态',
  //   require: false,
  //   complete: true,
  //   layout: 'flex',
  //   value: 'FUNCINFO_FORM_STATE',
  //   data: [
  //     { value: 'edit', label: '编辑' },
  //     { value: 'look', label: '查看' },
  //   ],
  // },
  // {
  //   type: 'radio',
  //   label: '弹出时表单位置',
  //   require: false,
  //   complete: true,
  //   layout: 'flex',
  //   value: 'FUNCINFO_FORM_POSITION',
  //   data: [
  //     { value: '0', label: '居中弹出' },
  //     { value: '1', label: '右侧滑入' },
  //   ],
  // },
  // {
  //   type: 'radio',
  //   label: '弹出时字段间距',
  //   require: false,
  //   complete: true,
  //   layout: 'flex',
  //   value: 'FUNCINFO_FIELD_SPACING',
  //   data: [
  //     { value: '0', label: '默认紧凑' },
  //     { value: '1', label: '默认标准' },
  //   ],
  // },
  // {
  //   type: 'checkbox',
  //   label: '弹出时表单显示子功能',
  //   require: false,
  //   complete: true,
  //   layout: 'block',
  //   remarks: '勾选后当表单被弹出时，子功能会显示否则不会。',
  //   value: 'FUNCINFO_FORMWINCHILD',
  //   text: '',
  //   data: [{ label: '', value: '1' }],
  // },
  {
    type: 'switch',
    label: '隐藏工具条',
    require: false,
    layout: 'block',
    remarks: '开启后按钮工具条将会被隐藏，重新打开可以选择子系统功能作为入口激活功能配置界面。',
    value: 'FUNCINFO_HIDEFORMTBAR',
  },
  {
    type: 'switch',
    label: '快速定位',
    require: false,
    layout: 'block',
    remarks:
      '系统会以分组框/子功能标题作为索引，对其进行快速定位操作(表单宽度过小定位栏会自动隐藏)。',
    value: 'FUNCINFO_GROUP_LOCATION',
  },
  // {
  //   type: 'switch',
  //   label: '启用科技灰皮肤',
  //   require: false,
  //   complete: true,
  //   layout: 'block',
  //   remarks: '开启后表单字段会以新的样式展现。',
  //   value: 'FUNCINFO_TABLESTYLE',
  // },
  // {
  //   type: 'switch',
  //   label: '展示审批记录',
  //   require: false,
  //   layout: 'block',
  //   remarks: '开启后工作流的审批记录会在表单的下方展示。',
  //   value: 'FUNCINFO_USEWFLOG',
  // },
  // {
  //   type: 'switch',
  //   label: '手动切换表单展示方案',
  //   require: false,
  //   complete: true,
  //   layout: 'block',
  //   remarks: '开启后表单取消手动切换展示方案按钮，靠程序或流程节点控制。',
  //   value: 'FUNCINFO_FORM_SHOW_WAY',
  // },
  // {
  //   type: 'switch',
  //   label: '手动切换表单打印方案',
  //   require: false,
  //   complete: true,
  //   layout: 'block',
  //   remarks: '开启后表单取消手动切换打印方案按钮，靠程序或流程节点控制。',
  //   value: 'FUNCINFO_FORM_PRINT_WAY',
  // },
];
// end

// 移动端配置
export const MOBLIDE_CONFIG = [
  // {
  //   type: 'select',
  //   label: '字段标题位置',
  //   layout: 'flex',
  //   require: false,
  //   configInfo: 'JE_FUNC_FIELD_ALIGN,FUNCINFO_APPFORM_LABEL_ALIGN,code,S',
  //   value: 'FUNCINFO_APPFORM_LABEL_ALIGN',
  // },
  {
    type: 'inputNumber',
    label: '字段标题宽',
    require: true,
    layout: 'flex',
    value: 'FUNCINFO_APPFORM_LABEL_WIDTH',
    suffix: 'px',
  },
  {
    type: 'switch',
    label: '隐藏表单子功能导航',
    require: false,
    value: 'FUNCINFO_APPFORM_DISABLE_CRUMB',
    layout: 'block',
    describe:
      '开启后，隐藏功能表单页顶部子功能导航条。适用于子功能仅列表直看数据，无需进入表单时使用。',
  },
  {
    type: 'qrcode',
    layout: 'block',
    label: '扫码预览',
    size: 200,
    value: 'QRCODE',
  },
];
// end
export const RESOURCECOLUMN_COLUMNQUERYTYPE = [
  { value: '=', label: '精确' },
  { value: 'like', label: '模糊' },
  // { value: 'like%', label: '后模糊' },
  { value: 'no', label: '无' },
];

export const RESOURCECOLUMN_LOCKED = [
  { value: 'before', label: '锁定列前' },
  { value: 'after', label: '锁定列后' },
];

// export const RESOURCECOLUMN_XTYPE = [
//   { value: 'uxcolumn', label: '数据列' },
//   // { value: 'uxcheckcolumn', label: 'checkbox列' }, // 去掉了
//   { value: 'actioncolumn', label: 'action列' },
//   { value: 'morecolumn', label: '多表头' },
//   { value: 'rownumberer', label: '序号' },
//   { value: 'markcolumn', label: '标记列' },
//   { value: 'postilcolumn', label: '批注列' },
//   { value: 'displayfield', label: '虚拟字段' },
// ];

// export const RESOURCEFIELD_XTYPE = [
//   { code: 'textfield', text: '文本框' },
//   { code: 'numberfield', text: '数值框' },
//   { code: 'rgroup', text: '单选框' },
//   { code: 'cgroup', text: '复选框' },
//   { code: 'cbbfield', text: '下拉框' },
//   { code: 'textarea', text: '文本域' },
//   { code: 'ckeditor', text: 'HTML编辑器' },
//   { code: 'textcode', text: '编号' },
//   { code: 'uxfilefield', text: '附件' },
//   { code: 'uxfilesfield', text: '多附件' },
//   { code: 'datefield', text: '日期' },
//   // { code: 'rangedatefield', text: '12.日期区间' },
//   { code: 'clocktimefield', text: '时间' },
//   // { code: 'rangeclocktimefield', text: '14.时间区间' },
//   { code: 'treessfield', text: '树形选择' },
//   { code: 'gridssfield', text: '关联选择' },
//   // { code: 'searchfield', text: '查询选择' },
//   { code: 'vueuserfield', text: '人员选择' },
//   { code: 'childfuncfield', text: '子功能集合' },
//   { code: 'colorfield', text: '颜色选择器' },
//   { code: 'displayfield', text: '虚拟字段' },
//   { code: 'starfield', text: '评星' },
//   { code: 'pinyinfield', text: '拼音' },
//   // { code: 'barfield', text: '进度条' },
//   { code: 'child', text: '子功能' },
//   { code: 'fieldset', text: '分组框' },
//   { code: 'imagepickerfield', text: '图片选择器' },
//   { code: 'codeeditor', text: '代码编辑器' },
//   { code: 'iconfield', text: '图标选择器' },
//   { code: 'jsonarrayfield', text: '对象集合' },
//   { code: 'workflowhistory', text: '审批记录' },
// ];

//现在由数据字典维护
// export const RESOURCEFIELD_VALUE = [
//   { value: '@USER_NAME@', label: '登录用户' },
//   { value: '@USER_CODE@', label: '登录用户编码' },
//   { value: '@USER_ID@', label: '登录用户ID' },
//   { value: '@DEPT_NAME@', label: '登录部门' },
//   { value: '@DEPT_CODE@', label: '登录部门编码' },
//   { value: '@DEPT_ID@', label: '登录部门ID' },
//   { value: '@DEPT_PATH@', label: '登陆部门路径' },
//   { value: '@USER_JTGSMC@', label: '登录集团公司' },
//   { value: '@USER.dept.jtgsDm@', label: '登录集团公司编码' },
//   { value: '@USER_COMPANY_NAME@', label: '登录公司' },
//   { value: '@USER_COMPANY_CODE@', label: '登录公司编码' },
//   { value: '@USER_COMPANY_ID@', label: '登录公司ID' },
//   { value: '@USER_JTGSID@', label: '登录集团公司ID' },
//   { value: '@NOW_DATE@', label: '当前日期(年月日)' },
//   { value: '@NOW_TIME@', label: '当前日期(年月日时分秒)' },
//   { value: '@NOW_YEAR@', label: '当前年(年)' },
//   { value: '@NOW_MONTH@', label: '当前月(月)' },
//   { value: '@USER.companyEmail@', label: '登录用户邮箱' },
//   { value: '@USER.phone@', label: '登录用户电话' },
//   { value: '@USER.ORGANIZATION_NAME@', label: '登录机构名称' },
//   { value: '@USER.ORGANIZATION_CODE@', label: '登录机构编码' },
//   { value: '@USER.ORGANIZATION_ID@', label: '登录机构ID' },
// ];

export const FIELDTYPE = [
  { code: 'ID', text: '主键', disabled: true },
  { code: 'VARCHAR30', text: '字符串(30)' },
  { code: 'VARCHAR50', text: '字符串(50)' },
  { code: 'VARCHAR100', text: '字符串(100)' },
  { code: 'VARCHAR255', text: '字符串(255)' },
  { code: 'VARCHAR767', text: '字符串(767)' },
  { code: 'VARCHAR1000', text: '字符串(1000)' },
  { code: 'VARCHAR2000', text: '字符串(2000)' },
  { code: 'VARCHAR4000', text: '字符串(4000)' },
  { code: 'VARCHAR', text: '字符串' },
  { code: 'FOREIGNKEY', text: '外键' },
  { code: 'NUMBER', text: '整数' },
  { code: 'DATE', text: '日期' },
  { code: 'DATETIME', text: '日期时间' },
  { code: 'FLOAT', text: '小数' },
  { code: 'FLOAT2', text: '小数(2)' },
  { code: 'YESORNO', text: '是非' },
  { code: 'CLOB', text: '超大文本' },
  { code: 'BIGCLOB', text: '巨大文本' },
  { code: 'CUSTOM', text: '自定义' },
];

export const SQLFILTER_CN_OPTIONS = [
  { value: 'and', label: '并且' },
  { value: 'or', label: '或者' },
];

export const LISTEBER_EDITOR_CONSTANT = {
  grid: {
    text: '列表',
    onId: 'grid',
    jsColumn: 'FUNCINFO_GRIDJSLISTENER',
    tableCode: 'JE_CORE_FUNCINFO',
    pkColumn: 'JE_CORE_FUNCINFO_ID',
  },
  form: {
    text: '表单',
    onId: 'form',
    jsColumn: 'FUNCINFO_FORMJSLISTENER',
    tableCode: 'JE_CORE_FUNCINFO',
    pkColumn: 'JE_CORE_FUNCINFO_ID',
  },
  column: {
    text: 'RESOURCECOLUMN_NAME',
    onId: 'JE_CORE_RESOURCECOLUMN_ID',
    jsColumn: 'RESOURCECOLUMN_JSLISTENER',
    tableCode: 'JE_CORE_RESOURCECOLUMN',
    pkColumn: 'JE_CORE_RESOURCECOLUMN_ID',
  },
  field: {
    text: 'RESOURCEFIELD_NAME',
    onId: 'JE_CORE_RESOURCEFIELD_ID',
    jsColumn: 'RESOURCEFIELD_JSLISTENER',
    tableCode: 'JE_CORE_RESOURCEFIELD',
    pkColumn: 'JE_CORE_RESOURCEFIELD_ID',
  },
  button: {
    text: 'RESOURCEBUTTON_NAME',
    onId: 'JE_CORE_RESOURCEBUTTON_ID',
    jsColumn: 'RESOURCEBUTTON_JSLISTENER',
    tableCode: 'JE_CORE_RESOURCEBUTTON',
    pkColumn: 'JE_CORE_RESOURCEBUTTON_ID',
  },
  actioncolumn: {
    text: 'RESOURCEBUTTON_NAME',
    onId: 'JE_CORE_RESOURCEBUTTON_ID',
    jsColumn: 'RESOURCEBUTTON_JSLISTENER',
    tableCode: 'JE_CORE_RESOURCEBUTTON',
    pkColumn: 'JE_CORE_RESOURCEBUTTON_ID',
  },
};
