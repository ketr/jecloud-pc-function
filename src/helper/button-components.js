import { defineAsyncComponent } from 'vue';

export const BUTTON_COMPONENTS = {
  AddButton: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/components/button/components/add-button.vue'),
  }),
  ButtonEvent: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/components/button/components/button-event.vue'),
  }),
  Cls: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/components/button/components/cls.vue'),
  }),
  Interpreter: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/components/button/components/interpreter.vue'),
  }),
};
