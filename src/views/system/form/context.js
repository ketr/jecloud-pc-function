import { inject, provide } from 'vue';

export const FunctionContextKey = Symbol('jeFunctionContextKey');

export const useProvideFunction = (state) => {
  provide(FunctionContextKey, state);
};

export const useInjectFunction = () => {
  return inject(FunctionContextKey, null);
};
