/*--------------------------------------- 配置菜单 ---------------------------------------*/

// export const AppConfig = [
//   {
//     text: '移动配置',
//     layout: 'flex',
//     background: 'linear-gradient(103deg,#F2E5FF 30%, rgba(179,169,169,0.00) 100%)',
//     children: [
//       { key: 'FuncAppMain', text: '手机配置', icon: 'fal fa-cog', color: '#FC5554', origin: 'app' },
//       {
//         key: 'MobileBaseConfig',
//         text: '基础配置',
//         icon: 'fal fa-cog',
//         color: '#FF6A10',
//         origin: 'app',
//       },
//       {
//         key: 'MobileListConfig',
//         text: '列表配置',
//         icon: 'fal fa-cog',
//         color: '#FF6A10',
//         origin: 'app',
//       },
//       {
//         text: '表单配置',
//         icon: 'jeicon jeicon-from',
//         color: '#DC4FF7',
//         key: 'FuncFormFields',
//         origin: 'app',
//       },
//       {
//         key: 'FuncButton',
//         text: ' 按钮配置',
//         icon: 'fal fa-mouse',
//         color: '#FF6381',
//         origin: 'app',
//       },
//       {
//         key: 'FuncChildren',
//         text: '子功能',
//         icon: 'jeicon jeicon-subfunction',
//         color: '#00BE84',
//         origin: 'app',
//       },
//     ],
//   },
// ];
export const AppConfig = [
  {
    text: '移动配置',
    layout: 'flex',
    background: 'linear-gradient(103deg,#F2E5FF 30%, rgba(179,169,169,0.00) 100%)',
    children: [
      { key: 'FuncAppMain', text: '手机配置', icon: 'fal fa-cog', color: '#FC5554', origin: 'app' },
      {
        key: 'MobileBaseConfig',
        text: '基础配置',
        icon: 'fal fa-cog',
        color: '#FF6A10',
      },
      {
        key: 'MobileListConfig',
        text: '列表配置',
        icon: 'fal fa-cog',
        color: '#FF6A10',
      },
      {
        text: '表单配置',
        icon: 'jeicon jeicon-from',
        color: '#DC4FF7',
        key: 'MobileFuncFormFields',
      },
      {
        key: 'MobileFuncButton',
        text: ' 按钮配置',
        icon: 'fal fa-mouse',
        color: '#FF6381',
      },
      {
        key: 'MobileFuncChildren',
        text: '子功能',
        icon: 'jeicon jeicon-subfunction',
        color: '#00BE84',
      },
    ],
  },
];

export const MenuConfig = [
  {
    text: '功能配置',
    background: 'linear-gradient(103deg,#fce7e7 30%, rgba(179,169,169,0.00) 100%)',
    children: [
      { key: 'FuncMain', text: '功能配置', icon: 'fal fa-cog', color: '#FC5554' },
      { key: 'FuncSimple', text: '简易配置', icon: 'jeicon jeicon-function', color: '#3CCE95' },
      { key: 'FuncButton', text: ' 按钮配置', icon: 'fal fa-mouse', color: '#FF6381' },
      {
        key: 'FuncChildren',
        text: '子功能项',
        icon: 'jeicon jeicon-subfunction',
        color: '#00BE84',
      },
      { key: 'FuncListeners', text: '功能事件', icon: 'fal fa-bolt', color: '#8BD2FC' },
      { key: 'DataLimitConfig', text: '数据权限', icon: 'jeicon jeicon-seal', color: '#7070E0' },
      // { key: '', text: '功能打包?', icon: 'fal fa-folder-download', color: '#FF8888' },
      { key: 'FuncHelp', text: '使用说明', icon: 'fal fa-question-circle', color: '#7070E0' },
    ],
  },
  // '-',
  {
    text: '列表配置',
    background: 'linear-gradient(103deg,#e7effc 30%, rgba(179,169,169,0.00) 100%)',
    children: [
      {
        text: '列表配置',
        icon: 'jeicon jeicon-table',
        color: '#F88F32',
        key: 'FuncListBase',
      },
      {
        text: '字段配置',
        icon: 'jeicon jeicon-table',
        color: '#00BE84',
        key: 'FuncListFields',
      },
      // {
      //   key: 'FuncListCustom',
      //   text: '自定义列表',
      //   icon: 'jeicon jeicon-table',
      //   color: '#7070E0',
      //   complete: true,
      // },
      // { key: 'FuncListPrint', text: '列表打印', icon: 'fal fa-print', color: '#FF6381' },
      {
        key: 'FuncQueryStrategy',
        text: '查询策略',
        icon: 'jeicon jeicon-query-strategy',
        color: '#8BD2FC',
      },
      {
        key: 'FuncQueryAdvanced',
        text: '高级查询',
        icon: 'jeicon jeicon-advanced-search',
        color: '#FB464E',
      },
    ],
  },
  // '-',
  {
    text: '表单配置',
    background: 'linear-gradient(103deg,#fcf8e7 30%, rgba(179,169,169,0.00) 100%)',
    children: [
      {
        key: 'FuncFormBase',
        text: '表单配置',
        icon: 'jeicon jeicon-from',
        color: '#5091C6',
      },
      {
        text: '字段配置',
        icon: 'jeicon jeicon-from',
        color: '#DC4FF7',
        key: 'FuncFormFields',
      },
      {
        text: '界面设计',
        icon: 'fal fa-tools',
        color: '#FB464E',
        key: 'FuncFormDesign',
      },
      // { key: 'FuncFormPrint', text: '表单打印', icon: 'fal fa-print', color: '#7EB31A' },
    ],
  },

  // '-',
  {
    text: '其他配置',
    background: 'linear-gradient(103deg,#e7fcf1 30%, rgba(179,169,169,0.00) 100%)',
    children: [
      {
        key: 'WorkflowCongig',
        text: '创建流程',
        icon: 'jeicon jeicon-treetable',
        color: '#FC5554',
      },
      // { key: '', text: '个人脚本库', icon: 'jeicon jeicon-js', color: '#7EB31A', complete: true },
      // {
      //   key: 'FuncDataFlow',
      //   text: '数据流转',
      //   icon: 'jeicon jeicon-datainterface',
      //   color: '#8BD2FC',
      // },
      {
        key: 'FuncSqlBuilder',
        text: '查询拼装器',
        icon: 'fal fa-puzzle-piece',
        color: '#FDD171',
      },
      {
        key: 'openTable',
        text: '打开资源表',
        icon: 'fal fa-database',
        color: '#DC4FF7',
      },
      {
        key: 'openFunction',
        text: '应用中心',
        icon: 'fal fa-server',
        color: '#8BD2FC',
      },
    ],
  },
].concat(AppConfig);

export const ModalConfig = {
  FuncSqlBuilder: {
    width: 1000,
    height: 'auto',
    cancelButton: '关闭',
    openTune: true,
  },
  FuncMain: {
    width: '95%',
    height: '95%',
    headerStyle: { height: '50px' },
    bodyStyle: { padding: '20px' },
  },
  FuncQueryStrategy: {
    width: 1315,
    height: 600,
    cancelButton: '关闭',
  },
  FuncQueryAdvanced: {
    width: 1400,
    height: 600,
    // bodyStyle: { paddingBottom: '80px' },
  },
  FuncSimple: {
    width: 500,
    drawer: true,
  },
  FuncListField: {
    title: '列表字段配置',
    width: 550,
    drawer: true,
  },
  FuncFormField: {
    title: '表单字段配置',
    width: 620,
    drawer: true,
  },
  WorkflowCongig: {
    micro: true,
  },
  DataLimitConfig: {
    micro: true,
  },
  openTable: {
    micro: true,
  },
  openFunction: {
    micro: true,
  },
  FuncListeners: {
    class: 'listener-modal',
  },
  MobileListConfig: {
    class: 'mobile-list-config-container',
  },
  FuncAppMain: {
    class: 'mobile-list-config-container',
  },
};
