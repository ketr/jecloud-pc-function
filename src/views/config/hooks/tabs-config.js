/*--------------------------------------- 功能配置 ---------------------------------------*/
export const FuncTabs = [
  {
    text: '基础配置',
    key: 'FuncBase',
  },
  // {
  //   text: '数据流转策略',
  //   key: 'FuncDataFlow',
  // },
  {
    text: '使用说明',
    key: 'FuncHelp',
  },
];
/*--------------------------------------- 表单配置 ---------------------------------------*/
export const FuncFormTabs = [
  {
    text: '基础配置',
    key: 'FuncFormBase',
  },
  {
    text: '字段配置',
    key: 'FuncFormFields',
  },
  {
    text: '界面设计',
    key: 'FuncFormDesign',
  },
  // {
  //   text: '打印规划',
  //   key: 'FuncFormPrint',
  // },
];

/*--------------------------------------- 列表配置 ---------------------------------------*/
export const FuncListTabs = [
  {
    text: '基础配置',
    key: 'FuncListBase',
  },
  {
    text: '字段配置',
    key: 'FuncListFields',
  },
  // {
  //   text: '自定义列表',
  //   key: 'FuncListCustom',
  //   complate: true,
  // },
  // {
  //   text: '打印规划',
  //   key: 'FuncListPrint',
  // },
];

/*--------------------------------------- 主界面 ---------------------------------------*/
export const FuncMainTabs = [
  {
    text: '功能',
    key: 'FuncBaseLayout',
    icon: 'jeicon jeicon-function',
  },
  {
    text: '列表',
    key: 'FuncListLayout',
    icon: 'fal fa-th-list',
  },
  {
    text: '表单',
    key: 'FuncFormLayout',
    icon: 'jeicon jeicon-from',
  },
  {
    text: '按钮',
    icon: 'fal fa-mouse',
    key: 'FuncButton',
  },
  {
    text: '子功能',
    icon: 'jeicon jeicon-subfunction',
    key: 'FuncChildren',
  },
];

/*--------------------------------------- App规划主界面 ---------------------------------------*/
export const FuncAppMainTabs = [
  {
    text: '功能',
    key: 'MobileBaseConfig',
    icon: 'jeicon jeicon-function',
  },
  {
    text: '列表',
    key: 'MobileListConfig',
    icon: 'fal fa-th-list',
  },
  {
    text: '表单',
    key: 'FuncFormFields',
    icon: 'jeicon jeicon-from',
  },
  {
    text: '按钮',
    icon: 'fal fa-mouse',
    key: 'FuncButton',
  },
  {
    text: '子功能',
    icon: 'jeicon jeicon-subfunction',
    key: 'FuncChildren',
  },
];
