import { Loading as loadingComponent } from '@jecloud/ui';
import { defineAsyncComponent } from 'vue';
/*--------------------------------------- 功能配置 ---------------------------------------*/
export const FuncBase = defineAsyncComponent({
  loader: () => import('./func-base.vue'),
  loadingComponent,
  delay: 50,
});
export const FuncDataFlow = defineAsyncComponent({
  loader: () => import('./func-data-flow.vue'),
  loadingComponent,
  delay: 50,
});
export const FuncHelp = defineAsyncComponent({
  loader: () => import('./func-help.vue'),
  loadingComponent,
  delay: 50,
});
export const FuncSimple = defineAsyncComponent({
  loader: () => import('./func-simple.vue'),
  loadingComponent,
  delay: 50,
});
export const FuncMain = defineAsyncComponent({
  loader: () => import('./func/index.vue'),
  loadingComponent,
  delay: 50,
});
export const FuncBaseLayout = defineAsyncComponent({
  loader: () => import('./func/func-layout.vue'),
  loadingComponent,
  delay: 50,
});
export const FuncListLayout = defineAsyncComponent({
  loader: () => import('./func/list-layout.vue'),
  loadingComponent,
  delay: 50,
});
export const FuncFormLayout = defineAsyncComponent({
  loader: () => import('./func/form-layout.vue'),
  loadingComponent,
  delay: 50,
});
/*--------------------------------------- 表单配置 ---------------------------------------*/
export const FuncFormBase = defineAsyncComponent({
  loader: () => import('./func-form-base.vue'),
  loadingComponent,
  delay: 50,
});
export const FuncFormFields = defineAsyncComponent({
  loader: () => import('./func-form-fields.vue'),
  loadingComponent,
  delay: 50,
});
export const FuncFormField = defineAsyncComponent({
  loader: () => import('./func-form-field.vue'),
  loadingComponent,
  delay: 50,
});
export const FuncFormDesign = defineAsyncComponent({
  loader: () => import('./func-form-design.vue'),
  loadingComponent,
  delay: 50,
});
export const FuncFormPrint = defineAsyncComponent({
  loader: () => import('./func-form-print.vue'),
  loadingComponent,
  delay: 50,
});

/*--------------------------------------- 列表配置 ---------------------------------------*/
export const FuncListBase = defineAsyncComponent({
  loader: () => import('./func-list-base.vue'),
  loadingComponent,
  delay: 50,
});
export const FuncListFields = defineAsyncComponent({
  loader: () => import('./func-list-fields.vue'),
  loadingComponent,
  delay: 50,
});
export const FuncListField = defineAsyncComponent({
  loader: () => import('./func-list-field.vue'),
  loadingComponent,
  delay: 50,
});
export const FuncListCustom = defineAsyncComponent({
  loader: () => import('./func-list-custom.vue'),
  loadingComponent,
  delay: 50,
});
export const FuncListPrint = defineAsyncComponent({
  loader: () => import('./func-list-print.vue'),
  loadingComponent,
  delay: 50,
});

/*--------------------------------------- 按钮配置 ---------------------------------------*/
export const FuncButton = defineAsyncComponent({
  loader: () => import('./func-button.vue'),
  loadingComponent,
  delay: 50,
});

/*--------------------------------------- 子功能配置 ---------------------------------------*/
export const FuncChildren = defineAsyncComponent({
  loader: () => import('./func-children.vue'),
  loadingComponent,
  delay: 50,
});

/*--------------------------------------- 插件 ---------------------------------------*/
export const FuncListeners = defineAsyncComponent({
  loader: () => import('./func-listeners.vue'),
  loadingComponent,
  delay: 50,
});
// export const FuncSqlBuilder = defineAsyncComponent({
//   loader: () => import('./func-sql-builder.vue'),
//   loadingComponent,
//   delay: 50,
// });
export const FuncQueryAdvanced = defineAsyncComponent({
  loader: () => import('./func-query-advanced.vue'),
  loadingComponent,
  delay: 50,
});
export const FuncQueryStrategy = defineAsyncComponent({
  loader: () => import('./func-query-strategy.vue'),
  loadingComponent,
  delay: 50,
});

// 移动端配置基础页面
export const MobileBaseConfig = defineAsyncComponent({
  loader: () => import('./mobile-base-config.vue'),
  loadingComponent,
  delay: 50,
});

export const MobileListConfig = defineAsyncComponent({
  loader: () => import('./mobile-list-config.vue'),
  loadingComponent,
  delay: 50,
});

export const FuncAppMain = defineAsyncComponent({
  loader: () => import('./app/index.vue'),
  loadingComponent,
  delay: 50,
});
