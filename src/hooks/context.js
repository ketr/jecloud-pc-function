import { inject, provide } from 'vue';

export const ListContextKey = Symbol('jeFuncConfigListContextKey');
export const FormContextKey = Symbol('jeFuncConfigFormContextKey');
export const appListContextKey = Symbol('jeFuncConfigAppListContextKey');

export const useProvideList = (state) => {
  provide(ListContextKey, state);
};

export const useInjectList = () => {
  return inject(ListContextKey, null);
};
export const useProvideForm = (state) => {
  provide(FormContextKey, state);
};

export const useInjectForm = () => {
  return inject(FormContextKey, null);
};

export const useProvideAppList = (state) => {
  provide(appListContextKey, state);
};

export const useInjectAppList = () => {
  return inject(appListContextKey, null);
};
