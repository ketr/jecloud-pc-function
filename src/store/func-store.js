import { defineStore } from 'pinia';

export const useFuncStore = defineStore({
  id: 'func-store',
  state: () => ({
    funcBean: null, // 功能bean数据
    funcFormBean: {}, // 功能表单bean数据
    activeProduct: null, // 当前激活的产品信息
    eventData: null, // 事件基础数据
    groupFields: [], //分组框字段
    groupColumns: [], //多表头字段
    attachFields: [], // 依附字段
  }),
  getters: {
    /**
     * 产品信息
     * @param {*} state
     * @returns
     */
    product(state) {
      return {
        id: state.activeProduct?.JE_PRODUCT_MANAGE_ID ?? state.activeProduct?.id,
        code: state.activeProduct?.PRODUCT_CODE ?? state.activeProduct?.code,
        name: state.activeProduct?.PRODUCT_NAME ?? state.activeProduct?.name,
      };
    },
  },
  actions: {
    setProduct(product) {
      this.activeProduct = product;
    },
  },
});
