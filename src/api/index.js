/**
 * 用于编写api方法
 * api url 统一在urls.js中声明导出使用，与方法分开
 */

import { ajax } from '@jecloud/utils';
import { useFuncStore } from '@/store/func-store';

import {
  GET_FUNCTION_LOADGRIDTREE,
  ADD_CHILD_FUNCTION,
  DELETE_CHILD_CHILDREN_RECORD,
  SEARCH_TABLE_LIST,
  CLEAN_FUNCTION_CACHE,
  UPDATE_TABLE_ITEM_FUNCTION,
  UPDATE_LABEL_PODITION,
  COPY_FUNCTION,
  REMOVE_FUNCTION_DATA,
  GET_FUNCTION_PERM,
  GET_FUNCTION_DATA,
  GET_PLUGIN_DATA,
  ADD_PLUGIN_DOSAVE,
  REMOVE_PLUGIN_DATA,
  GET_PLUGIN_ITEM_CONFIG,
  GET_FORM_JS_TREE_DATA,
  LOAD_DATA,
  DO_FORM_SYNC,
  DO_FORM_REMOVE,
  DO_FORM_UPDATE_LIST,
  DO_FORM_SAVE,
  GET_DADA_DICITEM_BY_CODE,
  LIST_BASE_CONFIG,
  BASE_CONFIG_SAVE,
  CHANGR_TRANSLATE_SQL,
  GET_PRODUCT_DATA,
  FUNCTION_ASYNC_CONFIG,
  CHILD_FUNC_GET_DATA,
  CHILD_FUNC_GET_DEFAULT_DATA,
  CHILD_FUNC_IMPORT_DATA,
  CHILD_FUNC_UPDATE,
  CHILD_FUNC_CUSTOMIZE_SAVE,
  CHILD_FUNC_REMOVE,
  CHILD_FUNC_FILED_SAVE,
  CHILD_FUNC_FILED_UPDATE,
  CHILD_FUNC_FILED_REMOVE,
  CHILD_FUNC_PERM,
  DATA_MOVE_GET,
  DATA_MOVE_SAVE,
  DATA_MOVE_UPDATE,
  DATA_MOVE_REMOVE,
  BUTTON_SAVE,
  BUTTON_UPDATE,
  BUTTON_REMOVE,
  BUTTON_PERM,
  LIST_FIELD_CONFIG_GET,
  LIST_FIELD_CONFIG_IMPORT,
  LIST_FIELD_CONFIG_ADD_MORECOLUMN,
  LIST_FIELD_CONFIG_ADD_NORMAL,
  LIST_FIELD_CONFIG_UPDATE,
  LIST_FIELD_CONFIG_REMOVE,
  LIST_FIELD_CONFIG_SYNC,
  LIST_FIELD_CONFIG_CREATE_INDEX,
  LIST_FIELD_CONFIG_REMOVE_INDEX,
  API_TABLE_VIEWCOLUMNCODE,
  API_TABLE_ADDCOLUMNBYDDLIST,
  API_TABLE_ADDCOLUMNBYDD,
  API_TABLE_SELECTONE,
  API_TABLE_GETCOLUMNDATA,
  API_TABLE_ADDCOLUMNBYTABLE,
  LIST_FIELD_CONFIG_DOSAVE,
  QYERY_STRATEGY_INSTER,
  QYERY_STRATEGY_DELETE,
  QYERY_STRATEGY_UPDATE,
  QYERY_STRATEGY_LOAD,
  LIST_ITEM_CUSTOM_GET_DETAIL,
  LIST_ITEM_CUSTOM_SAVE,
  OD_INSERT_UPDATE_LIST,
  DO_REMOVE,
  TABLE_DRAG,
  DD_DETAIL,
  LISTENER_EDITOR_GETJSENENT,
  DO_UPDATE,
  LIST_PRINT_SAVEDATA,
  FORM_PRINT_SAVEDATA,
  FORM_PRINT_GETDETAIL,
  API_TABLE_GETFUNCINFOBYTABLE,
  API_TABLE_ADDTABLEASSISTCOLUMN,
  API_FORM_FIELD_CONFIG_LOADMULTIMODELS,
  GET_FUNCTION_REFRESH,
  QUERY_ADCANCED_SAVE,
  QUERY_ADCANCED_LOAD,
  INTELLIGENT_TRANSITION,
  API_CLEAR_CACHE,
  API_DIC_CONFIG_INFO,
  GET_CHILDREN_FUNCTION_DATA,
  GET_CHILDREN_AND_PARENT_FUNCTION_DATA,
} from './urls';

/**
 * 但表单字段是子功能集合的时候，点击获取目标功能，从接口获取
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getSubChildList(params) {
  return ajax({ url: GET_CHILDREN_FUNCTION_DATA, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 智能切换查询选择和关联查询
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function SearchintelligentTransition(params) {
  return ajax({ url: INTELLIGENT_TRANSITION, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 入口页面table拖拽
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function MoveTable(params) {
  return ajax({ url: TABLE_DRAG, params, headers: { pd: 'meta' } }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

// 获取本功能的父功能及其子功能数据
export function GetParentAndChildrenData(params) {
  return ajax({ url: GET_CHILDREN_AND_PARENT_FUNCTION_DATA, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 功能字段配置同步
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function UpdateFunctionAsyncConfig(params) {
  return ajax({ url: FUNCTION_ASYNC_CONFIG, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 获取产品接口数据
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getProductData(params) {
  return ajax({ url: GET_PRODUCT_DATA, params }).then((info) => {
    if (info.success) {
      return info.data.rows;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 功能配置里面翻译调取接口（order && sql）
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function ChangeTranslateSql(params) {
  return ajax({ url: CHANGR_TRANSLATE_SQL, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 列表基础数据保存
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function baseConfigSave(params) {
  return ajax({ url: BASE_CONFIG_SAVE, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 获取功能基础配置的数据
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function GetListBaseConfig(params) {
  return ajax({ url: LIST_BASE_CONFIG, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 获取数据字典的数据
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function GetDicItemByCodeData(params) {
  return ajax({ url: GET_DADA_DICITEM_BY_CODE, params }).then((info) => {
    if (info != '') {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * je/load 获取数据
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getData(params, pd = 'meta') {
  // const funcStore = useFuncStore();
  return ajax({ url: LOAD_DATA, params, headers: { pd } }).then((info) => {
    if (info.success) {
      return info.data.rows;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * je/load 获取数据
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function queryAdvancedGetData(params) {
  return ajax({ url: QUERY_ADCANCED_LOAD, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 表单JS事件
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function GetFormJsTreeData(params) {
  return ajax({ url: GET_FORM_JS_TREE_DATA, params, headers: { pd: 'meta' } }).then((info) => {
    if (info != '') {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 业务插件点击删除
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function GetPluginItemConfig(params) {
  return ajax({ url: GET_PLUGIN_ITEM_CONFIG, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 业务插件点击删除
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function RemovePluginData(params) {
  return ajax({ url: REMOVE_PLUGIN_DATA, params, headers: { pd: 'meta' } }).then((info) => {
    if (info) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 业务插件点击添加
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function AddPluginDoSave(params) {
  return ajax({ url: ADD_PLUGIN_DOSAVE, params }).then((info) => {
    if (info != '') {
      return info.obj;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 获取业务插件的数据
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function GetPluginData(params) {
  return ajax({ url: GET_PLUGIN_DATA, params }).then((info) => {
    if (info != '') {
      return info.rows;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 获取首页数据刷新
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function GetFuncRefresh(params) {
  return ajax({ url: GET_FUNCTION_REFRESH, params }).then((info) => {
    if (info != '') {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 获取功能权限
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function GetFunctionPerm(param) {
  const params = {
    refresh: true,
    perm: 0,
  };
  Object.assign(params, param);
  return ajax({ url: GET_FUNCTION_PERM, params }).then((info) => {
    if (info != '') {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 获取功能数据权限
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function GetFunctionData(param) {
  const params = {
    refresh: true,
    perm: 0,
  };
  Object.assign(params, param);
  return ajax({ url: GET_FUNCTION_DATA, params }).then((info) => {
    if (info != '') {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 清空功能
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function RemoveFunctionData(params) {
  return ajax({ url: REMOVE_FUNCTION_DATA, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 复制功能
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function CopyFunction(params) {
  return ajax({ url: COPY_FUNCTION, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 修改功能列表Item
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function UpdateTableItemFunction(params) {
  return ajax({ url: UPDATE_TABLE_ITEM_FUNCTION, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 修改标签位置
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function updateLabelPosition(params) {
  return ajax({ url: UPDATE_LABEL_PODITION, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 清楚缓存
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function CleanFunctionCache(params) {
  return ajax({ url: CLEAN_FUNCTION_CACHE, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 获取子系统tree数据
 * @export
 * @param {String} 表编码 tableCode
 * @param {String} SQL查询扩展 j_query
 * @param {String} SQL查询扩展rootId
 * @param {String} 产品ID   SY_PRODUCT_ID
 * @return {Promise}
 */
export function GetFunctionTreeData(param) {
  const params = {
    tableCode: 'JE_CORE_FUNCINFO',
    // excludes: 'checked',
    rootId: 'ROOT',
    // limit: -1,
    // node: 'ROOT',
    SY_PRODUCT_ID: param.SY_PRODUCT_ID,
    j_query: JSON.stringify({
      order: [{ code: 'SY_ORDERINDEX', type: 'ASC' }],
      custom: [
        {
          type: 'and',
          value: [
            { type: 'like', code: 'FUNCINFO_FUNCNAME', value: param.searchValue, cn: 'or' },
            { type: 'like', code: 'FUNCINFO_FUNCCODE', value: param.searchValue, cn: 'or' },
          ],
        },
      ],
    }),
  };
  return ajax({ url: GET_FUNCTION_LOADGRIDTREE, params: { ...params } }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 添加子系统
 * @export
 * @param {Object} params.SY_PRODUCT_ID 产品ID
 * @param {Object} params.SY_PRODUCT_CODE 产品code
 * @param {Object} params.SY_PRODUCT_NAME 产品名称
 * @param {Object} params.FUNCINFO_FUNCCODE 功能编码
 * @param {Object} params.FUNCINFO_FUNCNAME 功能名称
 * @param {Object} params.FUNCINFO_ICON 图标样式
 * @param {Object} params.FUNCINFO_NODEINFOTYPE 节点信息类型
 * @param {Object} params.tableCode 表编码
 * @param {Object} params.SY_PARENT 父节点ID
 * @param {Object} params.SY_PATH 树形路径
 * @param {Object} params.节点类型 节点类型
 * @param {Object} params.SY_LAYER 层次
 * @param {Object} params.SY_LAYER 层次
 * @param {Object} params.SY_STATUS 数据状态
 * @param {Object} params.FUNCINFO_TABLENAME 表名
 * @param {Object} params.FUNCINFO_PKNAME 主键名称
 * @param {Object} params.FUNCINFO_DBNAME 数据源
 * @param {Object} params.FUNCINFO_PROCEDURE 存储过程
 * @param {Object} params.FUNCINFO_SQL SQL
 * @param {Object} params.FUNCINFO_QUERYPARAM 查询参数
 * @param {Object} params.FUNCINFO_FUNCREMARK 功能描述
 * @return {Promise}
 */
export function AddChildFunction(param) {
  const funcStore = useFuncStore();
  const params = {
    SY_STATUS: '1',
    tableCode: 'JE_CORE_FUNCINFO',
    SY_NODETYPE: 'GENERAL',
    SY_PRODUCT_ID: funcStore.product.id,
    SY_PRODUCT_CODE: funcStore.product.code,
    SY_PRODUCT_NAME: funcStore.product.name,
    ...param,
  };
  return ajax({ url: ADD_CHILD_FUNCTION, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 删除子系统数据
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function DeleteChildFunction(params) {
  // const params = {
  //   tableCode: 'JE_CORE_FUNCINFO',
  // };
  // Object.assign(params, param);
  return ajax({ url: DELETE_CHILD_CHILDREN_RECORD, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 点击表名获取表名列表数据
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function SearchTableList(params) {
  const funcStore = useFuncStore();
  params.SY_PRODUCT_ID = funcStore.product.id;
  return ajax({ url: SEARCH_TABLE_LIST, params }).then((info) => {
    if (info.success) {
      return info.data.rows;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 表单 与列表同步
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function DoFormSync(params) {
  return ajax({ url: DO_FORM_SYNC, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 删除表单
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function DoFormRemove(params) {
  return ajax({ url: DO_FORM_REMOVE, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 保存表单
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function DoFormUpdateList(params) {
  return ajax({ url: DO_FORM_UPDATE_LIST, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 添加表单
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function DoFormSave(params) {
  return ajax({ url: DO_FORM_SAVE, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 子功能列表数据加载
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function ChildFuncGetData(params) {
  return ajax({ url: CHILD_FUNC_GET_DATA, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 子功能 获取该功能下的子功能列表
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function ChildFuncGetDefaultData(params) {
  return ajax({ url: CHILD_FUNC_GET_DEFAULT_DATA, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 子功能导入
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function ChildFuncImportData(params) {
  return ajax({ url: CHILD_FUNC_IMPORT_DATA, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 子功能保存
 * @param {Object} params
 * @return {Promise}
 */
export function ChildFuncUpdate(params) {
  return ajax({ url: CHILD_FUNC_UPDATE, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 子功能自定义按钮添加
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function ChildFuncCustomizeSave(params) {
  return ajax({ url: CHILD_FUNC_CUSTOMIZE_SAVE, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 子功能删除
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function ChildFuncRemove(params) {
  return ajax({ url: CHILD_FUNC_REMOVE, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 子功能字段添加
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function ChildFuncFieldSave(params, headers) {
  return ajax({ url: CHILD_FUNC_FILED_SAVE, params, headers }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 子功能字段保存
 * @param {Object} params
 * @return {Promise}
 */
export function ChildFuncFieldUpdate(params) {
  return ajax({ url: CHILD_FUNC_FILED_UPDATE, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 子功能字段删除
 * @param {Object} params
 * @return {Promise}
 */
export function ChildFuncFieldRemove(params) {
  return ajax({ url: CHILD_FUNC_FILED_REMOVE, params, headers: { pd: 'meta' } }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 子功能授权
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function ChildFuncPerm(params) {
  return ajax({ url: CHILD_FUNC_PERM, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 数据流转策略获取
 * @param {Object} params
 * @return {Promise}
 */
export function DataMoveGet(params) {
  const funcStore = useFuncStore();
  return ajax({ url: DATA_MOVE_GET, params, headers: { pd: 'meta' } }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 数据流转策略添加
 * @param {Object} params
 * @return {Promise}
 */
export function DataMoveSave(params) {
  return ajax({ url: DATA_MOVE_SAVE, params, headers: { pd: 'meta' } }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 数据流转策略保存
 * @param {Object} params
 * @return {Promise}
 */
export function DataMoveUpdate(params) {
  return ajax({ url: DATA_MOVE_UPDATE, params, headers: { pd: 'meta' } }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 数据流转策略删除
 * @param {Object} params
 * @return {Promise}
 */
export function DataMoveRemove(params) {
  return ajax({ url: DATA_MOVE_REMOVE, params, headers: { pd: 'meta' } }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 按钮获取
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function ButtonGet(params) {
  return ajax({ url: GET_PLUGIN_ITEM_CONFIG, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 按钮添加
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function ButtonSave(params) {
  return ajax({ url: BUTTON_SAVE, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 按钮保存
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function ButtonUpdate(params) {
  return ajax({ url: BUTTON_UPDATE, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 按钮删除
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function ButtonRemove(params) {
  return ajax({ url: BUTTON_REMOVE, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 按钮授权
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function ButtonPerm(params) {
  return ajax({ url: BUTTON_PERM, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 列表字段配置获取
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function ListFieldConfigGet(params) {
  return ajax({ url: LIST_FIELD_CONFIG_GET, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 列表字段配置获取导入
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function ListFieldConfigImport(params) {
  return ajax({ url: LIST_FIELD_CONFIG_IMPORT, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 列表字段配置添加(多表头)
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function ListFieldConfigAddMorecolumn(params) {
  return ajax({ url: LIST_FIELD_CONFIG_ADD_MORECOLUMN, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 列表字段配置添加(普通)
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function ListFieldConfigAddNormal(params, headers) {
  return ajax({ url: LIST_FIELD_CONFIG_ADD_NORMAL, params, headers }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 列表字段配置更新
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function ListFieldConfigUpdate(params) {
  return ajax({ url: LIST_FIELD_CONFIG_UPDATE, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 列表字段配置删除
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function ListFieldConfigRemove(params) {
  return ajax({ url: LIST_FIELD_CONFIG_REMOVE, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 列表字段配置与表单同步
 * @param {Object} params
 * @return {Promise}
 */
export function ListFieldConfigSync(params) {
  return ajax({ url: LIST_FIELD_CONFIG_SYNC, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 列表字段配置创建索引
 * @param {Object} params
 * @return {Promise}
 */
export function ListFieldConfigCreateIndex(params, headers) {
  return ajax({ url: LIST_FIELD_CONFIG_CREATE_INDEX, params, headers }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 列表字段配置删除索引
 * @param {Object} params
 * @return {Promise}
 */
export function ListFieldConfigRemoveIndex(params, headers) {
  return ajax({ url: LIST_FIELD_CONFIG_REMOVE_INDEX, params, headers }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 表格列添加字段判重
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function viewColumnCode(params) {
  // const funcStore = useFuncStore();
  return ajax({
    url: API_TABLE_VIEWCOLUMNCODE,
    params: params,
    // headers: { pd: funcStore.product.code },
  }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 表格列级联辅助字典添加字段
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function addColumnByDDList(params) {
  const funcStore = useFuncStore();
  return ajax({
    url: API_TABLE_ADDCOLUMNBYDDLIST,
    params: params,
    headers: { pd: funcStore.product.code },
  }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 表格列辅助字典添加字段
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function addColumnByDD(params) {
  const funcStore = useFuncStore();
  return ajax({
    url: API_TABLE_ADDCOLUMNBYDD,
    params: params,
    headers: { pd: funcStore.product.code },
  }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 *
 * 人员辅助表单数据查询
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function selectOne(params) {
  return ajax({ url: API_TABLE_SELECTONE, params: params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 获得表格列数据
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getColumnData(params) {
  return ajax({ url: API_TABLE_GETCOLUMNDATA, params: params }).then((info) => {
    if (info != '') {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 *
 * 人员辅助字段添加
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function addColumnByTable(params) {
  const funcStore = useFuncStore();
  return ajax({
    url: API_TABLE_ADDCOLUMNBYTABLE,
    params: params,
    headers: { pd: funcStore.product.code },
  }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 *
 * 添加标记列和批注列
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function addSignAndPostilColumn(params) {
  return ajax({ url: LIST_FIELD_CONFIG_DOSAVE, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 *
 * 查询策略添加
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function addQueryStrategy(params) {
  return ajax({ url: QYERY_STRATEGY_INSTER, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 *
 * 查询策略删除
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function DeleteQueryStrategy(params) {
  return ajax({ url: QYERY_STRATEGY_DELETE, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 *
 * 查询策略修改
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function updateQueryStrategy(params) {
  return ajax({ url: QYERY_STRATEGY_UPDATE, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 查询策略load
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getQueryStrategy(params) {
  return ajax({ url: QYERY_STRATEGY_LOAD, params: params }).then((info) => {
    if (info != '') {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 列表 自定义列表 获取详情
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function ListItemCustomGetDetail(params) {
  return ajax({ url: LIST_ITEM_CUSTOM_GET_DETAIL, params: params }).then((info) => {
    if (info != '') {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 列表 自定义列表 保存
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function ListItemCustomSave(params) {
  return ajax({ url: LIST_ITEM_CUSTOM_SAVE, params: params, headers: { pd: 'meta' } }).then(
    (info) => {
      if (info != '') {
        return info;
      } else {
        return Promise.reject(info);
      }
    },
  );
}

/**
 * 公共批量修改添加接口
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function doInsertUpdateList(params) {
  return ajax({ url: OD_INSERT_UPDATE_LIST, params: params, headers: { pd: 'meta' } }).then(
    (info) => {
      if (info != '') {
        return info;
      } else {
        return Promise.reject(info);
      }
    },
  );
}

/**
 * 高级查询保存数据
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function queryAdvancedSave(params) {
  return ajax({ url: QUERY_ADCANCED_SAVE, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 公共删除接口
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function doRemove(params) {
  return ajax({ url: DO_REMOVE, params: params, headers: { pd: 'meta' } }).then((info) => {
    if (info != '') {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 获得功能脚本的数据
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getJsEvent(params) {
  return ajax({ url: LISTENER_EDITOR_GETJSENENT, params: params, headers: { pd: 'meta' } }).then(
    (info) => {
      if (info != '') {
        return info.data;
      } else {
        return Promise.reject(info);
      }
    },
  );
}

/**
 * 获得功能脚本的数据
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function doUpdate(params) {
  return ajax({ url: DO_UPDATE, params: params, headers: { pd: 'meta' } }).then((info) => {
    if (info != '') {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 数据字典详情
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function DataDictionaryDetail(params) {
  return ajax({ url: DD_DETAIL, params: params }).then((info) => {
    if (info != '') {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 数据字典详情
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function doUpdateHelp(params) {
  return ajax({ url: LIST_PRINT_SAVEDATA, params: params }).then((info) => {
    if (info != '') {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 表单打印保存数据
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function formPrintSaveData(params) {
  return ajax({ url: FORM_PRINT_SAVEDATA, params: params }).then((info) => {
    if (info != '') {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 表单打印获取详情
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function formPrintGetDetail(params) {
  return ajax({ url: FORM_PRINT_GETDETAIL, params: params }).then((info) => {
    if (info != '') {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 *
 * 查询表创建的所有功能
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getFuncInfoByTable(params) {
  return ajax({ url: API_TABLE_GETFUNCINFOBYTABLE, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 *
 * 表辅助添加数据接口
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function addTableAssistColumn(params) {
  const funcStore = useFuncStore();
  return ajax({
    url: API_TABLE_ADDTABLEASSISTCOLUMN,
    params: params,
    headers: { pd: funcStore.product.code },
  }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 *
 * 查询表的字段信息
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function loadTabelColumn(params) {
  return ajax({ url: API_FORM_FIELD_CONFIG_LOADMULTIMODELS, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 *
 * 查清空字典缓存
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function clearCache(params) {
  return ajax({ url: API_CLEAR_CACHE, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 *
 * 获得表单字典字典的配置
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getDicConfigInfo(params) {
  return ajax({ url: API_DIC_CONFIG_INFO, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
