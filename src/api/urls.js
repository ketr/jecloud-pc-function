/**
 * API_URL命名规则：API_模块_方法
 */

// 获取子系统列表数据
export const GET_FUNCTION_LOADGRIDTREE = '/je/meta/funcInfo/loadGridTreeData';
// 添加子系统
export const ADD_CHILD_FUNCTION = '/je/meta/funcInfo/doSave';
// 删除子系统的列表
export const DELETE_CHILD_CHILDREN_RECORD = '/je/meta/funcInfo/doRemove';
// 查询表名的接口列表数据
export const SEARCH_TABLE_LIST = '/je/meta/resourceTable/table/load';
// 清空缓存
export const CLEAN_FUNCTION_CACHE = '/je/meta/staticize/clearStaticize';
// 编辑功能信息
// export const UPDATE_TABLE_ITEM_FUNCTION = '/je/meta/funcInfo/doUpdate';
export const UPDATE_TABLE_ITEM_FUNCTION = '/je/meta/funcInfo/doUpdateFuncCode';
// 标签位置整体设置
export const UPDATE_LABEL_PODITION = '/je/meta/resField/updateLabelPosition';
// 复制功能
export const COPY_FUNCTION = '/je/meta/funcInfo/funcCopy';
// 清空操作
export const REMOVE_FUNCTION_DATA = '/je/meta/funcInfo/clearFuncInfo';
// 获取功能权限
export const GET_FUNCTION_PERM = '/je/meta/funcInfo/getStaticFuncPermByCode';
// 获取功能数据
export const GET_FUNCTION_DATA = '/je/meta/funcInfo/getStaticFuncByCode';
// 首页刷新接口refresh
export const GET_FUNCTION_REFRESH = '/je/meta/staticize/getStaticFuncByCode';
// 获取业务插件数据
export const GET_PLUGIN_DATA = '/je/meta/funcConfig/load';
// 业务插件中点击添加
export const ADD_PLUGIN_DOSAVE = '/je/meta/funcConfig/doSave';
// 业务插件中删除点击删除
export const REMOVE_PLUGIN_DATA = '/je/common/doRemove';
// 业务插件点击列表的配置项调取的接口
export const GET_PLUGIN_ITEM_CONFIG = '/je/meta/funcInfo/loadFunc';
// 表单JS事件中
export const GET_FORM_JS_TREE_DATA = '/je/common/getTree';
// 加载数据
export const LOAD_DATA = '/je/common/load';

// 表单 与列表同步
export const DO_FORM_SYNC = '/je/meta/resField/doSync';
// 删除表单
export const DO_FORM_REMOVE = '/je/meta/resField/doRemove';
// 保存表单
export const DO_FORM_UPDATE_LIST = '/je/meta/resField/doUpdateList';
// 添加表单
export const DO_FORM_SAVE = '/je/meta/resField/doSave';
// 获取数据字典数据
export const GET_DADA_DICITEM_BY_CODE = '/je/meta/dd/dd/getDicItemByCode';

// 获取功能基础配置的数据
export const LIST_BASE_CONFIG = '/je/meta/funcInfo/getInformationById';
// 基础配置保存
export const BASE_CONFIG_SAVE = '/je/meta/funcInfo/doUpdate';

// 功能配置里面翻译调取接口（order && sql）
export const CHANGR_TRANSLATE_SQL = '/je/meta/funcInfo/translate';
// 获取产品的数据
export const GET_PRODUCT_DATA = '/je/meta/product/load';
// 功能配置中字段配置同步
export const FUNCTION_ASYNC_CONFIG = '/je/meta/funcInfo/syncConfig';

// 子功能列表数据加载
export const CHILD_FUNC_GET_DATA = '/je/meta/funcInfo/loadFunc';
// 子功能获取该功能下的子功能列表
export const CHILD_FUNC_GET_DEFAULT_DATA = '/je/meta/funcInfo/load';
// 子功能导入
export const CHILD_FUNC_IMPORT_DATA = '/je/meta/funRelation/doImpl';
// 子功能保存
export const CHILD_FUNC_UPDATE = '/je/meta/funRelation/doUpdateList';
// 子功能自定义按钮添加
export const CHILD_FUNC_CUSTOMIZE_SAVE = '/je/meta/funRelation/doSave';
// 子功能删除
export const CHILD_FUNC_REMOVE = '/je/meta/funRelation/doRemove';
// 子功能字段添加
export const CHILD_FUNC_FILED_SAVE = '/je/meta/funRelation/saveRelationSet';
// 子功能字段保存
export const CHILD_FUNC_FILED_UPDATE = '/je/meta/associationField/doUpdateList';
// 子功能字段删除
export const CHILD_FUNC_FILED_REMOVE = '/je/common/doRemove';
// 子功能授权
export const CHILD_FUNC_PERM = '/je/rbac/permission/quickGranSubFuncPerm';

// 数据流转策略获取
export const DATA_MOVE_GET = '/je/common/load';
// 数据流转策略添加
export const DATA_MOVE_SAVE = '/je/common/doSave';
// 数据流转策略保存
export const DATA_MOVE_UPDATE = '/je/common/doUpdateList';
// 数据流转策略删除
export const DATA_MOVE_REMOVE = '/je/common/doRemove';

// 按钮添加
export const BUTTON_SAVE = '/je/meta/resButton/doSave';
// 按钮保存
export const BUTTON_UPDATE = '/je/meta/resButton/doUpdateList';
// 按钮删除
export const BUTTON_REMOVE = '/je/meta/resButton/doRemove';
// 授权按钮
export const BUTTON_PERM = '/je/rbac/permission/quickGranFuncButtonPerm';

// 列表字段配置获取
export const LIST_FIELD_CONFIG_GET = '/je/meta/funcInfo/loadFunc';
// 列表字段配置导入
export const LIST_FIELD_CONFIG_IMPORT = '/je/meta/resColumn/impl';
// 列表字段配置添加(多表头)
export const LIST_FIELD_CONFIG_ADD_MORECOLUMN = '/je/meta/resColumn/doSave';
// 列表字段配置添加(普通)
export const LIST_FIELD_CONFIG_ADD_NORMAL = '/je/meta/resourceTable/table/column/addField';
// 列表字段配置更新
export const LIST_FIELD_CONFIG_UPDATE = '/je/meta/resColumn/doUpdateList';
// 列表字段配置删除
export const LIST_FIELD_CONFIG_REMOVE = '/je/meta/resColumn/doRemove';
// 列表字段配置与表单同步
export const LIST_FIELD_CONFIG_SYNC = '/je/meta/resColumn/doSync';
// 列表字段配置创建索引
export const LIST_FIELD_CONFIG_CREATE_INDEX =
  '/je/meta/resourceTable/table/tableIndex/createIndexByColumn';
// 列表字段配置删除索引
export const LIST_FIELD_CONFIG_REMOVE_INDEX =
  '/je/meta/resourceTable/table/tableIndex/removeIndexByColumn';

//表格列添加字段判重
export const API_TABLE_VIEWCOLUMNCODE = '/je/meta/resourceTable/table/column/checkUnique';
//表格列级联字典辅助添加字段
export const API_TABLE_ADDCOLUMNBYDDLIST =
  '/je/meta/resourceTable/table/column/addAuxiliaryCasacdeDdColumn';
//表格列辅助字典添加字段
export const API_TABLE_ADDCOLUMNBYDD = '/je/meta/resourceTable/table/column/addAuxiliaryDdColumn';
//人员辅助表单数据查询
export const API_TABLE_SELECTONE = '/je/meta/resourceTable/selectOne';
//获得表格列数据
export const API_TABLE_GETCOLUMNDATA = '/je/meta/resourceTable/table/column/load';
//人员辅助字段添加
export const API_TABLE_ADDCOLUMNBYTABLE =
  '/je/meta/resourceTable/table/column/addAuxiliaryUserColumn';
//添加标记列和批注列
export const LIST_FIELD_CONFIG_DOSAVE = '/je/meta/resColumn/doSave';

//查询策略添加
export const QYERY_STRATEGY_INSTER = '/je/meta/queryStrategy/doSave';
//查询策略删除
export const QYERY_STRATEGY_DELETE = '/je/meta/queryStrategy/doRemove';
//查询策略修改
export const QYERY_STRATEGY_UPDATE = '/je/meta/queryStrategy/doUpdateList';
//查询策略load
export const QYERY_STRATEGY_LOAD = '/je/meta/queryStrategy/load';

// 列表 自定义列表 获取详情
export const LIST_ITEM_CUSTOM_GET_DETAIL = '/je/meta/funcInfo/getInfoById';
// 列表 自定义列表 保存
export const LIST_ITEM_CUSTOM_SAVE = '/je/common/doUpdate';
//公共批量修改添加接口
export const OD_INSERT_UPDATE_LIST = '/je/common/doInsertUpdateList';
//公共删除接口
export const DO_REMOVE = '/je/common/doRemove';

// 表格的拖拽
export const TABLE_DRAG = '/je/common/move';

// 数据字典详情
export const DD_DETAIL = '/je/meta/dd/dd/initLoad';

// export const TABLE_DRAG = '/je/meta/resourceTable/move';

//获得功能脚本的数据
export const LISTENER_EDITOR_GETJSENENT = '/je/meta/funcInfo/getJsEvent';
//功能脚本数据保存
export const DO_UPDATE = '/je/common/doUpdate';

//列表打印保存数据
export const LIST_PRINT_SAVEDATA = '/je/meta/funcInfo/doUpdateHelp';

//表单打印保存数据
export const FORM_PRINT_SAVEDATA = '/je/meta/funcInfo/doUpdateInfo';

// 表单打印获取详情
export const FORM_PRINT_GETDETAIL = '/je/meta/funcInfo/getInfoById';

//查询表创建的所有功能
export const API_TABLE_GETFUNCINFOBYTABLE = '/je/meta/resourceTable/table/getFuncInfoByTable';

//表辅助添加数据接口
export const API_TABLE_ADDTABLEASSISTCOLUMN =
  '/je/meta/resourceTable/table/column/addAuxiliaryTableColumn';

//查询表的字段信息
export const API_FORM_FIELD_CONFIG_LOADMULTIMODELS = '/je/meta/doAct/doAct/loadMultiModels';

// 高级查询保存数据
export const QUERY_ADCANCED_SAVE = '/je/meta/advancedQuery/doUpdateList';
// 高级查询加载数据
export const QUERY_ADCANCED_LOAD = '/je/meta/advancedQuery/getData';

// 智能切换的英文
export const INTELLIGENT_TRANSITION = '/je/meta/resColumn/getRelevanceField';

// 清空字典缓存
export const API_CLEAR_CACHE = '/je/meta/dictionary/clearCache';

// 获得表单字典字典的配置
export const API_DIC_CONFIG_INFO = '/je/meta/createConfig/dicConfigInfo';

// 获取集合的数据
export const GET_CHILDREN_FUNCTION_DATA = '/je/meta/funRelation/getChildList';

// 编辑器获取左侧本功能的父功能及其子功能数据
export const GET_CHILDREN_AND_PARENT_FUNCTION_DATA = '/je/meta/funRelation/getParentAndChildInfo';
